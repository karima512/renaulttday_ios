//
//  TbordController.h
//  RenaultTday
//
//  Created by MacBook Pro on 06/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//
//

#import <UIKit/UIKit.h>

@interface TbordController : UIViewController<UIScrollViewDelegate>{
    IBOutlet UIButton *LeftButton;
    IBOutlet UIButton *RightButton;
    BOOL leftShown;
    BOOL rightShown;
    NSArray *animationArray;
    
    
}
@property (strong, nonatomic) IBOutlet UILabel *textAteliers;
@property (strong, nonatomic) IBOutlet UILabel *textMenus;
@property (strong, nonatomic) IBOutlet UILabel *textIndex;

@property (strong, nonatomic) IBOutlet UIView<UIScrollViewDelegate> *popup1;
@property (nonatomic,strong) IBOutlet UIImageView *imageView1_1;
@property (nonatomic,strong) IBOutlet UIImageView *imageView1_2_1;
@property (nonatomic,strong) IBOutlet UIImageView *imageView1_2_2;



@property (strong, nonatomic) IBOutlet UIView<UIScrollViewDelegate> *popup2;
@property (nonatomic,strong) IBOutlet UIImageView *imageView2_1;
@property (nonatomic,strong) IBOutlet UIImageView *imageView2_3;

@property (strong, nonatomic) IBOutlet UIView<UIScrollViewDelegate> *popup3;

@property (strong, nonatomic) IBOutlet UIView<UIScrollViewDelegate> *popup4;

@property (strong, nonatomic) IBOutlet UIView<UIScrollViewDelegate> *popup5;
@property (nonatomic,strong) IBOutlet UIImageView *imageView5_1;
@property (nonatomic,strong) IBOutlet UIImageView *imageView5_2;
@property (nonatomic,strong) IBOutlet UIImageView *imageView5_3;
@property (nonatomic,strong) IBOutlet UIImageView *imageView5_4;



@property (strong, nonatomic) IBOutlet UIView<UIScrollViewDelegate> *popup6;



@property (strong, nonatomic) IBOutlet UIView *leftMenu;
@property (strong, nonatomic) IBOutlet UIView *rightMenu;
@property (strong, nonatomic) IBOutlet UIImageView *bgOpacity;

@property (strong, nonatomic) IBOutlet UIScrollView *pageScroll1;
@property (strong, nonatomic) IBOutlet UIScrollView *pageScroll2;
@property (strong, nonatomic) IBOutlet UIScrollView *pageScroll3;
@property (strong, nonatomic) IBOutlet UIScrollView *pageScroll4;
@property (strong, nonatomic) IBOutlet UIScrollView *pageScroll5;
@property (strong, nonatomic) IBOutlet UIScrollView *pageScroll6;

@property (strong, nonatomic) IBOutlet UIPageControl *pageControl1;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl2;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl3;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl4;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl5;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl6;

@property (strong, nonatomic) IBOutlet UIButton *btn1;
@property (strong, nonatomic) IBOutlet UIButton *btn2;
@property (strong, nonatomic) IBOutlet UIButton *btn3;
@property (strong, nonatomic) IBOutlet UIButton *btn4;
@property (strong, nonatomic) IBOutlet UIButton *btn5;
@property (strong, nonatomic) IBOutlet UIButton *btn6;

- (IBAction) showPopUp: (id)sender;
- (IBAction) hidePopUp: (id)sender;
- (IBAction) MenuRight: (id)sender;
- (IBAction) MenuLeft: (id)sender;

- (IBAction) BackHome: (id)sender;


@end

//
//  RegisterController.h
//  RenaultTday
//
//  Created by Karim Jebali on 06/11/13.
//
//

#import <UIKit/UIKit.h>

@interface RegisterController : UIViewController
@property (strong, nonatomic) IBOutlet UITextField *mail;
@property (strong, nonatomic) IBOutlet UITextField *password;
@property (strong, nonatomic) IBOutlet UITextField *nom;
@property (strong, nonatomic) IBOutlet UITextField *prenom;
@property (strong, nonatomic) IBOutlet UITextField *groupe;

-(IBAction)register:(id)sender;
-(IBAction)backLogin:(id)sender;

@end

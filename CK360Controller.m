//
//  CK360Controller.m
//  RenaultTday
//
//  Created by MacBook Pro on 06/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//
//

#import "CK360Controller.h"
#import "ViewController.h"
#import "HomeController.h"
#import "MenuLeftViewController.h"
#import "MenuRightViewControllerG1.h"
#import "MenuRightViewControllerG2.h"
#import "MenuRightViewControllerG3.h"
#import <QuartzCore/QuartzCore.h>

@interface CK360Controller ()

@end

@implementation CK360Controller

@synthesize textAteliers;
@synthesize textMenus;
//@synthesize menuController = _menuController;
@synthesize leftMenu , rightMenu , bgOpacity,textIndex,viewC,viewK,change;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    NSLog(@"full memory");
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle
- (void)viewDidUnload {
    self.textAteliers = nil;
    self.textIndex = nil;
    self.textMenus = nil;
    self.leftMenu = nil;
    self.rightMenu = nil;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    //[self addChildViewController:leftMenu];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *menu = @"360";
    [defaults setObject:menu forKey:@"menu"];
    [defaults synchronize];
    NSString *cat = @"";
    NSString *current_cat = [defaults objectForKey:@"category"];
    if ([current_cat isEqualToString:@"T" ]) {
        cat= [defaults objectForKey:@"menu_label_T"];
    }else if ([current_cat isEqualToString:@"CK" ]) {
        cat= [defaults objectForKey:@"menu_label_CK"];
    }
    else if ([current_cat isEqualToString:@"D" ]) {
        cat= [defaults objectForKey:@"menu_label_D"];
    }
    textIndex.text = [NSString stringWithFormat:@"%@ / %@",cat,[defaults objectForKey:@"atelier_label_360"]];
    
    self.leftMenu.alpha = 0;
    self.leftMenu.frame = CGRectMake (0, 0, 342, 768);
    
    leftShown = NO;
    
    self.rightMenu.alpha = 0;
     self.rightMenu.frame = CGRectMake (679, 0, 342, 768);
    rightShown = NO;
    //rightMenu.userInteractionEnabled = YES ;
    
    textAteliers.transform = CGAffineTransformMakeRotation(M_PI / 2);
    textMenus.transform = CGAffineTransformMakeRotation(-M_PI / 2);
    // hide C
    viewC.alpha = 0;
    GammeC = NO; // gammeK
	//Set slides extension
	[imageSquence1 setExtension:@"png"];
	
	//Set slide prefix prefix
	[imageSquence1 setPrefix:@"K"];
	
	//Set number of slides
	[imageSquence1 setNumberOfImages:59];
    [imageSquence1 setIncrement:1];
    
    //Set slides extension
	[imageSquence2 setExtension:@"png"];
	
	//Set slide prefix prefix
	[imageSquence2 setPrefix:@"C"];
	
	//Set number of slides
	[imageSquence2 setNumberOfImages:59];
    [imageSquence2 setIncrement:1];
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    if(!GammeC){
    if(imageSquence1.increment== 0)
		imageSquence1.increment = 1;
	[super touchesBegan:touches withEvent:event];
	
    UITouch *touch = [[event allTouches] anyObject]  ;
    
    CGPoint touchLocation = [touch locationInView:self->imageSquence1];
    if ([imageSquence1 pointInside:touchLocation withEvent:event]) {
        
        NSLog(@"%f", touchLocation.x);
        previous = touchLocation.x;
    }
    }
    else
    {
        if(imageSquence2.increment== 0)
            imageSquence2.increment = 1;
        [super touchesBegan:touches withEvent:event];
        
        UITouch *touch = [[event allTouches] anyObject]  ;
        
        CGPoint touchLocation = [touch locationInView:self->imageSquence2];
        if ([imageSquence2 pointInside:touchLocation withEvent:event]) {
            
            NSLog(@"%f", touchLocation.x);
            previous = touchLocation.x;
    }
    }
    
    UITouch *touchLeft = [[event allTouches] anyObject]  ;
    
    CGPoint touchLocationLeft = [touchLeft locationInView:self->leftMenu];
    if (![leftMenu pointInside:touchLocationLeft withEvent:event] && leftShown ) {
        [self animateLeftHide];
    }
    
    UITouch *touchRight = [[event allTouches] anyObject]  ;
    
    CGPoint touchLocationRight = [touchRight locationInView:self->rightMenu];
    if (![rightMenu pointInside:touchLocationRight withEvent:event] && rightShown ) {
        [self animateRightHide];
    }
    
}

-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event {
	[super touchesMoved:touches withEvent:event];
	
    UITouch *touch = [[event allTouches] anyObject];
    if(!GammeC){

    CGPoint touchLocation = [touch locationInView:self->imageSquence1];
    if ([imageSquence1 pointInside:touchLocation withEvent:event]) {
        int location = touchLocation.x;
        
        if(location < previous)
            current += imageSquence1.increment;
        else
            current -= imageSquence1.increment;
        
        previous = location;
        NSLog(@"curent %d", location);
        
        if(current > imageSquence1.numberOfImages)
            current = 0;
        if(current < 0)
            current = imageSquence1.numberOfImages;
        
        NSString *path = [NSString stringWithFormat:@"%@%d", imageSquence1.prefix, current];
        NSLog(@"%@", path);
        
        path = [[NSBundle mainBundle] pathForResource:path ofType:imageSquence1.extension];
        
        
        UIImage *img =  [[UIImage alloc] initWithContentsOfFile:path];
        
        [imageSquence1 setImage:img];
        
    }
    }else
    {
        CGPoint touchLocation = [touch locationInView:self->imageSquence2];
        if ([imageSquence2 pointInside:touchLocation withEvent:event]) {
            int location = touchLocation.x;
            
            if(location < previous)
                current += imageSquence2.increment;
            else
                current -= imageSquence2.increment;
            
            previous = location;
            NSLog(@"curent %d", location);
            
            if(current > imageSquence2.numberOfImages)
                current = 0;
            if(current < 0)
                current = imageSquence2.numberOfImages;
            
            NSString *path = [NSString stringWithFormat:@"%@%d", imageSquence2.prefix, current];
            NSLog(@"%@", path);
            
            path = [[NSBundle mainBundle] pathForResource:path ofType:imageSquence2.extension];
            
            
            UIImage *img =  [[UIImage alloc] initWithContentsOfFile:path];
            
            [imageSquence2 setImage:img];
            
    }
    }
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight)
		return YES;
	return NO;
}
-(IBAction)BackHome:(id)sender{
    HomeController *view = [[HomeController alloc] initWithNibName:@"HomeController" bundle:nil];
    view.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:view animated:YES completion:nil];
    
}
- (void) animateLeftShow {
    //    [self addChildViewController:leftController];
    MenuLeftViewController *leftController = [[MenuLeftViewController alloc] init];
    leftController.view.frame = self.leftMenu.bounds;
    [self.leftMenu addSubview:leftController.view];
    [leftController didMoveToParentViewController:self];
    
    [self addChildViewController:leftController];
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    
    leftMenu.alpha = 1;
    bgOpacity.hidden = NO;
    // leftMenu.frame = CGRectMake (0, 0, 342, 768);
    
    leftShown = YES;
    
    [UIView commitAnimations];
}
- (void)animateLeftHide  {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    
    leftMenu.alpha = 0;
    bgOpacity.hidden = YES;
    
    // leftMenu.frame = CGRectMake (0, 0, 342, 768);
    leftShown = NO;
    [UIView commitAnimations];
}
- (void) animateRightShow {
    NSUserDefaults *__autoreleasing defaults = [NSUserDefaults standardUserDefaults];
    NSString  * __autoreleasing current_group = [defaults objectForKey:@"group"];
    
    if([current_group isEqualToString:@"1"]){
        MenuRightViewControllerG1 *rightController = [[MenuRightViewControllerG1 alloc] init];
        rightController.view.frame = self.rightMenu.bounds;
        
        //self.rightMenu = rightController.view;
        [self.rightMenu addSubview:rightController.view];
        [rightController didMoveToParentViewController:self];
        
        [self addChildViewController:rightController];
        
    }
    if([current_group isEqualToString:@"2"]){
        MenuRightViewControllerG2 *rightController = [[MenuRightViewControllerG2 alloc] init];
        rightController.view.frame = self.rightMenu.bounds;
        
        //self.rightMenu = rightController.view;
        [self.rightMenu addSubview:rightController.view];
        [rightController didMoveToParentViewController:self];
        
        [self addChildViewController:rightController];
        
    }
    if([current_group isEqualToString:@"3"]){
        MenuRightViewControllerG3 *rightController = [[MenuRightViewControllerG3 alloc] init];
        rightController.view.frame = self.rightMenu.bounds;
        
        //self.rightMenu = rightController.view;
        [self.rightMenu addSubview:rightController.view];
        [rightController didMoveToParentViewController:self];
        
        [self addChildViewController:rightController];
        
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    
    rightMenu.alpha = 1;
    bgOpacity.hidden = NO;
    //self.rightMenu.frame = CGRectMake (679, 0, 0, 768);
    
    rightShown = YES;
    
    [UIView commitAnimations];
}
- (void)animateRightHide  {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    
    rightMenu.alpha = 0;
    bgOpacity.hidden = YES;
    
    //rightMenu.frame = CGRectMake (679, 0, 0, 768);
    rightShown = NO;
    [UIView commitAnimations];
}



-(IBAction)MenuRight:(id)sender{
    [self animateRightShow];
    rightShown = YES;
}
-(IBAction)MenuLeft:(id)sender{
    NSLog(@"click here left");
    
    [self animateLeftShow];
    leftShown = YES;
    
}
-(IBAction)changeView:(id)sender{
    NSLog(@"click here to change");
    if(!GammeC){
        GammeC = YES;
        viewK.alpha = 0;
        viewC.alpha = 1;
        /*NSString *path = [NSString stringWithFormat:@"360k-petiteimage"];
        NSLog(@"%@", path);
        
        path = [[NSBundle mainBundle] pathForResource:path ofType:@"png"];
        
        
        UIImage *img =  [[UIImage alloc] initWithContentsOfFile:path];*/
        
        [change setImage:[UIImage imageNamed:@"360k-petiteimage.png"] forState:UIControlStateNormal];
        //[change setBackgroundImage:img];
        
        //[img release];

    }else{
        GammeC = NO;
        viewK.alpha = 1;
        viewC.alpha = 0;
        /*NSString *path = [NSString stringWithFormat:@"360-c-petite-image"];
        NSLog(@"%@", path);
        
        path = [[NSBundle mainBundle] pathForResource:path ofType:@"png"];
        
        
        UIImage *img =  [[UIImage alloc] initWithContentsOfFile:path];*/
        
        [change setImage:[UIImage imageNamed:@"360-c-petite-image.png"] forState:UIControlStateNormal];
        //[change setBackgroundImage:img];
        
        //[img release];

    }
    
}


@end

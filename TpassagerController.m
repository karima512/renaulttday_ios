//
//  TpassagerController.m
//  RenaultTday
//
//  Created by MacBook Pro on 06/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//
//

#import "TpassagerController.h"
#import "ViewController.h"
#import "HomeController.h"
#import "MenuLeftViewController.h"
#import "MenuRightViewControllerG1.h"
#import "MenuRightViewControllerG2.h"
#import "MenuRightViewControllerG3.h"
#import <QuartzCore/QuartzCore.h>


@interface TpassagerController ()

@end

@implementation TpassagerController {
    NSUserDefaults *defaults;
}

@synthesize textAteliers;
@synthesize textMenus, textIndex;
@synthesize popup1,popup2,popup3,popup4,popup5,popup6,popup7,popup8,popup9,popup10,popup11;
@synthesize imageView1_1,imageView5_1,imageView5_2,imageView4_2,imafeView5_2_tab,imageView5_6_tab;
@synthesize pageScroll1, pageScroll2,pageScroll3,pageScroll4,pageScroll5,pageScroll6,pageScroll7,pageScroll8,pageScroll9,pageScroll10,pageScroll11;
@synthesize pageControl1,pageControl2,pageControl3,pageControl4,pageControl5,pageControl6,pageControl7,pageControl8,pageControl9,pageControl10,pageControl11;
@synthesize leftMenu , rightMenu , bgOpacity;
@synthesize btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn10,btn11;
@synthesize imageView6_1,imageView6_2,imageView6_3,imageView6_4,imageView6_5,imageView6_6,imageView6_7,imageView6_8,imageView6_9,imageView6_10;
@synthesize label6_1,label6_2,label6_4,label6_5,label6_6,label6_8,label6_10,labelPlus;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    NSLog(@"full memory");
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)makeRoundedImage:(UIButton *) button

{
    UIImage * img = [UIImage imageNamed:@"bg_btn_middle.png"];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    
    NSString *titleString = [button currentTitle];  // get button title
    
    CGSize fontSize = [titleString sizeWithFont:[UIFont systemFontOfSize:18.0]];
    
    CGRect currentFrame = button.frame;
    
    CGRect buttonFrame = CGRectMake(currentFrame.origin.x, currentFrame.origin.y, fontSize.width + 30.0, fontSize.height + 12.0);
    button.frame = buttonFrame;
    
    button.clipsToBounds = YES;
    
    button.layer.cornerRadius = 10;
    
    button.layer.borderWidth=1.0f;
    
    [button setTitle:titleString forState: UIControlStateNormal];
    
    [button.titleLabel setFont:[UIFont fontWithName:@"DIN" size:15]];
    
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    defaults = [NSUserDefaults standardUserDefaults];
    NSString *menu = @"passager";
    [defaults setObject:menu forKey:@"menu"];
    [defaults synchronize];
    
    NSString *cat = @"";
    NSString *current_cat = [defaults objectForKey:@"category"];
    if ([current_cat isEqualToString:@"T" ]) {
        cat= [defaults objectForKey:@"menu_label_T"];
    }else if ([current_cat isEqualToString:@"CK" ]) {
        cat= [defaults objectForKey:@"menu_label_CK"];
    }
    else if ([current_cat isEqualToString:@"D" ]) {
        cat= [defaults objectForKey:@"menu_label_D"];
    }
    textIndex.text = [NSString stringWithFormat:@"%@ / %@",cat,[defaults objectForKey:@"atelier_label_passager"]];
    
    
    self.leftMenu.alpha = 0;
    self.leftMenu.frame = CGRectMake (0, 0, 342, 768);
    
    leftShown = NO;
    
    self.rightMenu.alpha = 0;
    // self.rightMenu.frame = CGRectMake (679, 0, 0, 768);
    rightShown = NO;
    //rightMenu.userInteractionEnabled = YES ;
    
    
    textAteliers.transform = CGAffineTransformMakeRotation(M_PI / 2);
    textMenus.transform = CGAffineTransformMakeRotation(-M_PI / 2);
    popup1.alpha = 0;
    popup2.alpha = 0;
    popup3.alpha = 0;
    popup4.alpha = 0;
    popup5.alpha = 0;
    popup6.alpha = 0;
    popup7.alpha = 0;
    popup8.alpha = 0;
    popup9.alpha = 0;
    popup10.alpha = 0;
    popup11.alpha = 0;
    
    // title num
    
    [self makeRoundedImage:btn1];
    [self makeRoundedImage:btn2];
    [self makeRoundedImage:btn3];
    [self makeRoundedImage:btn4];
    [self makeRoundedImage:btn5];
    [self makeRoundedImage:btn6];
    [self makeRoundedImage:btn7];
    [self makeRoundedImage:btn8];
    [self makeRoundedImage:btn9];
    [self makeRoundedImage:btn10];
    [self makeRoundedImage:btn11];
    
    //self.popup.delegate = self.delegate;
    [self.view addSubview:popup1];
    [self.view addSubview:popup2];
    [self.view addSubview:popup3];
    [self.view addSubview:popup4];
    [self.view addSubview:popup5];
    [self.view addSubview:popup6];
    [self.view addSubview:popup7];
    [self.view addSubview:popup8];
    [self.view addSubview:popup9];
    [self.view addSubview:popup10];
    [self.view addSubview:popup11];
    
    
    
    /* UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(200, 200, 14, 14)];
     imgView.image = [UIImage imageNamed:@"btn_slide_circle.png"];
     [popup addSubview:imgView];
     [imgView release];*/
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated{
    pageScroll1.delegate = self;
    pageControl1.numberOfPages = 1;
    pageControl1.currentPage = 0;
    
    pageScroll2.delegate = self;
    pageControl2.numberOfPages = 1;
    pageControl2.currentPage = 0;
    
    pageScroll3.delegate = self;
    pageControl3.numberOfPages = 1;
    pageControl3.currentPage = 0;
    
    pageScroll4.delegate = self;
    pageControl4.numberOfPages = 1;
    pageControl4.currentPage = 0;
    
    pageScroll5.delegate = self;
    pageControl5.numberOfPages = 9;
    pageControl5.currentPage = 0;
    
    pageScroll6.delegate = self;
    pageControl6.numberOfPages = 1;
    pageControl6.currentPage = 0;
    
    pageScroll7.delegate = self;
    pageControl7.numberOfPages = 1;
    pageControl7.currentPage = 0;

    pageScroll8.delegate = self;
    pageControl8.numberOfPages = 1;
    pageControl8.currentPage = 0;
    
    pageScroll9.delegate = self;
    pageControl9.numberOfPages = 1;
    pageControl9.currentPage = 0;
    
    pageScroll10.delegate = self;
    pageControl10.numberOfPages = 1;
    pageControl10.currentPage = 0;
    
    pageScroll11.delegate = self;
    pageControl11.numberOfPages = 1;
    pageControl11.currentPage = 0;


    
    pageScroll1.contentSize = CGSizeMake(self.popup1.frame.size.width * pageControl1.numberOfPages, self.popup1.frame.size.height);
    pageScroll2.contentSize = CGSizeMake(self.popup2.frame.size.width * pageControl2.numberOfPages, self.popup2.frame.size.height);
    pageScroll3.contentSize = CGSizeMake(self.popup3.frame.size.width * pageControl3.numberOfPages, self.popup3.frame.size.height);
    pageScroll4.contentSize = CGSizeMake(self.popup4.frame.size.width * pageControl4.numberOfPages, self.popup4.frame.size.height);
    pageScroll5.contentSize = CGSizeMake(self.popup5.frame.size.width * pageControl5.numberOfPages, self.popup5.frame.size.height);
    pageScroll6.contentSize = CGSizeMake(self.popup6.frame.size.width * pageControl6.numberOfPages, self.popup6.frame.size.height);
    pageScroll7.contentSize = CGSizeMake(self.popup7.frame.size.width * pageControl7.numberOfPages, self.popup7.frame.size.height);
    pageScroll8.contentSize = CGSizeMake(self.popup8.frame.size.width * pageControl8.numberOfPages, self.popup8.frame.size.height);
    pageScroll9.contentSize = CGSizeMake(self.popup9.frame.size.width * pageControl9.numberOfPages, self.popup9.frame.size.height);
    pageScroll10.contentSize = CGSizeMake(self.popup10.frame.size.width * pageControl10.numberOfPages, self.popup10.frame.size.height);
    pageScroll11.contentSize = CGSizeMake(self.popup11.frame.size.width * pageControl11.numberOfPages, self.popup11.frame.size.height);
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touchLeft = [[event allTouches] anyObject]  ;
    CGPoint touchLocationLeft = [touchLeft locationInView:self.leftMenu];
    NSLog(@"left %@",[NSValue valueWithCGPoint:touchLocationLeft]);
    
    if (![self.leftMenu pointInside:touchLocationLeft withEvent:event] && leftShown ) {
        [self animateLeftHide];
    }
    
    UITouch *touchRight = [[event allTouches] anyObject]  ;
    
    CGPoint touchLocationRight = [touchRight locationInView:self.rightMenu];
    NSLog(@"right %@",[NSValue valueWithCGPoint:touchLocationRight]);
    /* if ([self.rightMenu pointInside: [self.view convertPoint:touchLocationRight toView: rightMenu] withEvent:event]) {
     [self animateRightHide];
     NSLog(@"right 11");
     
     }*/
    
    if (![self.rightMenu pointInside:touchLocationRight withEvent:event] && rightShown ) {
        [self animateRightHide];
        NSLog(@"right 222");
    }
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight)
		return YES;
	return NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.textAteliers = nil;
    self.textIndex = nil;
    self.textMenus = nil;
    self.popup1 = nil;
    self.popup2 = nil;
    self.popup3 = nil;
    self.popup4 = nil;
    self.popup5 = nil;
    self.popup6 = nil;
    self.popup7 = nil;
    self.popup8 = nil;
    self.popup9 = nil;
    self.popup10 = nil;
    self.popup11 = nil;
    self.pageScroll1 = nil;
    self.pageScroll2 = nil;
    self.pageScroll3 = nil;
    self.pageScroll4 = nil;
    self.pageScroll5 = nil;
    self.pageScroll6 = nil;
    self.pageScroll7 = nil;
    self.pageScroll8 = nil;
    self.pageScroll9 = nil;
    self.pageScroll10 = nil;
    self.pageScroll11 = nil;

}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth1 = self.popup1.frame.size.width;
    int page = floor((pageScroll1.contentOffset.x - pageWidth1 / pageControl1.numberOfPages) / pageWidth1) + 1;
    NSLog(@"page ; %d",page);
    pageControl1.currentPage = page;
    CGFloat pageWidth2 = self.popup2.frame.size.width;
    int page2 = floor((pageScroll2.contentOffset.x - pageWidth2 / pageControl2.numberOfPages) / pageWidth2) + 1;
    NSLog(@"page ; %d",page2);
    pageControl2.currentPage = page2;
    
    CGFloat pageWidth3 = self.popup3.frame.size.width;
    int page3 = floor((pageScroll3.contentOffset.x - pageWidth3 / pageControl3.numberOfPages) / pageWidth3) + 1;
    NSLog(@"page ; %d",page2);
    pageControl3.currentPage = page3;
    
    CGFloat pageWidth4 = self.popup4.frame.size.width;
    int page4 = floor((pageScroll4.contentOffset.x - pageWidth4 / pageControl4.numberOfPages) / pageWidth4) + 1;
    NSLog(@"page ; %d",page4);
    pageControl4.currentPage = page4;
    
    CGFloat pageWidth5 = self.popup5.frame.size.width;
    int page5 = floor((pageScroll5.contentOffset.x - pageWidth5 / pageControl5.numberOfPages) / pageWidth5) + 1;
    NSLog(@"page ; %d",page5);
    pageControl5.currentPage = page5;
    
    CGFloat pageWidth6 = self.popup6.frame.size.width;
    int page6 = floor((pageScroll6.contentOffset.x - pageWidth6 / pageControl6.numberOfPages) / pageWidth6) + 1;
    NSLog(@"page ; %d",page6);
    pageControl6.currentPage = page6;

    CGFloat pageWidth7 = self.popup7.frame.size.width;
    int page7 = floor((pageScroll7.contentOffset.x - pageWidth7 / pageControl7.numberOfPages) / pageWidth7) + 1;
    NSLog(@"page ; %d",page7);
    pageControl7.currentPage = page7;
    
    CGFloat pageWidth8 = self.popup8.frame.size.width;
    int page8 = floor((pageScroll8.contentOffset.x - pageWidth8 / pageControl8.numberOfPages) / pageWidth8) + 1;
    NSLog(@"page ; %d",page8);
    pageControl8.currentPage = page8;

    CGFloat pageWidth9 = self.popup9.frame.size.width;
    int page9 = floor((pageScroll9.contentOffset.x - pageWidth9 / pageControl9.numberOfPages) / pageWidth9) + 1;
    NSLog(@"page ; %d",page9);
    pageControl9.currentPage = page9;

    CGFloat pageWidth10 = self.popup10.frame.size.width;
    int page10 = floor((pageScroll10.contentOffset.x - pageWidth10 / pageControl10.numberOfPages) / pageWidth10) + 1;
    NSLog(@"page ; %d",page10);
    pageControl10.currentPage = page10;

    CGFloat pageWidth11 = self.popup11.frame.size.width;
    int page11 = floor((pageScroll11.contentOffset.x - pageWidth11 / pageControl11.numberOfPages) / pageWidth11) + 1;
    NSLog(@"page ; %d",page11);
    pageControl11.currentPage = page11;

    
}


- (void) initPopUpView {
}

- (void) animatePopUpShow:(NSString *)popid {
    LeftButton.enabled = NO;
    RightButton.enabled = NO;

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(initPopUpView)];
    NSLog(@"%d",[popid intValue]);
    switch ([popid intValue]) {
        case 1:
            // Do some think here
            popup1.alpha = 1;
            popup1.frame = CGRectMake (55, 35, 950, 685);
            animationArray=[NSArray arrayWithObjects:
                            [UIImage imageNamed:@"P020132-modifie-en-vert.png"],
                            [UIImage imageNamed:@"P020132-modifie-en-vert-aveccercle.png"],
                            [UIImage imageNamed:@"P020132-modifie-en-vert-aveccercleetcadre.png"],
                            [UIImage imageNamed:@"P020130-coupe-slide2.png"],
                            nil];
            imageView1_1.animationImages=animationArray;
            imageView1_1.animationDuration=4;
            imageView1_1.animationRepeatCount=0;
            imageView1_1.image = [UIImage imageNamed:@"P020130-coupe-slide2.png"];
            [imageView1_1 startAnimating];
            break;
        case 2:
            // Do some think here
            popup3.alpha = 1;
            popup3.frame = CGRectMake (55, 35, 950, 685);
            
            break;
        case 3:
            // Do some think here
            popup4.alpha = 1;
            popup4.frame = CGRectMake (55, 35, 950, 685);
            
            break;
        case 4:
            // Do some think here
            //defaults = [NSUserDefaults standardUserDefaults];
            if([[defaults objectForKey:@"language"] isEqualToString:@"en"]) {
                imafeView5_2_tab.image = [UIImage imageNamed:@"Tpassager1-en.png"];
                imageView5_6_tab.image = [UIImage imageNamed:@"Tpassager1-en.png"];

            } else {
                imafeView5_2_tab.image = [UIImage imageNamed:@"tableau1-slide7.png"];
                imageView5_6_tab.image = [UIImage imageNamed:@"tableau1-slide7.png"];
            }
            
            popup5.alpha = 1;
            popup5.frame = CGRectMake (55, 35, 950, 685);
             animationArray=[NSArray arrayWithObjects:
                            [UIImage imageNamed:@"moteur1-Image1.png"],
                            [UIImage imageNamed:@"moteur1-Image2.png"],
                            [UIImage imageNamed:@"moteur1-Image3.png"],
                            [UIImage imageNamed:@"moteur1-Image4.png"],
                            [UIImage imageNamed:@"moteur1-Image5.png"],
                            [UIImage imageNamed:@"moteur1-Image6.png"],
                            nil];
            imageView5_1.animationImages=animationArray;
            imageView5_1.animationDuration=6;
            imageView5_1.animationRepeatCount=0;
            imageView5_1.image = [UIImage imageNamed:@"moteur1-Image6.png"];
            [imageView5_1 startAnimating];
            animationArray=[NSArray arrayWithObjects:
                            [UIImage imageNamed:@"moteur2-Image1.png"],
                            [UIImage imageNamed:@"moteur2-Image2.png"],
                            [UIImage imageNamed:@"moteur2-Image3.png"],
                            [UIImage imageNamed:@"moteur2-Image4.png"],
                            [UIImage imageNamed:@"moteur2-Image5.png"],
                            [UIImage imageNamed:@"moteur2-Image6.png"],
                            nil];
            imageView5_2.animationImages=animationArray;
            imageView5_2.animationDuration=6;
            imageView5_2.animationRepeatCount=0;
            imageView5_2.image = [UIImage imageNamed:@"moteur2-Image6.png"];
            [imageView5_2 startAnimating];
            
            break;
        case 5:
            // Do some think here
            popup6.alpha = 1;
            popup6.frame = CGRectMake (55, 35, 950, 685);
            [self performSelector:@selector(beginAnimationTick) withObject:nil afterDelay:0.5];

           

            break;
        case 6:
            // Do some think here
            popup7.alpha = 1;
            popup7.frame = CGRectMake (55, 35, 950, 685);
            
            break;
        case 7:
            // Do some think here
            popup8.alpha = 1;
            popup8.frame = CGRectMake (55, 35, 950, 685);
            
            break;
        case 8:
            // Do some think here
            popup9.alpha = 1;
            popup9.frame = CGRectMake (55, 35, 950, 685);
            
            break;
        case 9:
            // Do some think here
            popup10.alpha = 1;
            popup10.frame = CGRectMake (55, 35, 950, 685);
            
            break;
        case 10:
            // Do some think here
            popup11.alpha = 1;
            popup11.frame = CGRectMake (55, 35, 950, 685);
            
            break;
       /* case 11:
            // Do some think here
            popup11.alpha = 1;
            popup11.frame = CGRectMake (55, 35, 950, 685);
            
            break;*/
        default:
            NSLog(@"Default Message here");
            break;
    }
    //float popUpY = popup.frame.size.height;
    //float popUpX = popup.frame.size.width;
    //float viewY = self.view.frame.size.height;
    //float viewX = self.view.frame.size.width;
    
    [UIView commitAnimations];
}
- (void) animatePopUpHide:(int)popid {
    LeftButton.enabled = YES;
    RightButton.enabled = YES;

    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    NSLog(@"hide: %d",popid);
    switch (popid) {
        case 1:
            popup1.alpha = 0;
            //popup1.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 2:
            popup2.alpha = 0;
            //popup2.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 3:
            popup3.alpha = 0;
            //popup3.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 4:
            popup4.alpha = 0;
            //popup4.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 5:
            popup5.alpha = 0;
            //popup5.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 6:
            popup6.alpha = 0;
            //popup6.frame = CGRectMake (55, 35, 0, 0);
            imageView6_1.hidden = YES;
            imageView6_2.hidden = YES;
            imageView6_3.hidden = YES;
            imageView6_4.hidden = YES;
            imageView6_5.hidden = YES;
            imageView6_6.hidden = YES;
            imageView6_7.hidden = YES;
            imageView6_8.hidden = YES;
            imageView6_9.hidden = YES;
            imageView6_10.hidden = YES;
            label6_1.hidden = YES;
            label6_4.hidden = YES;
            label6_5.hidden = YES;
            label6_6.hidden = YES;
            label6_8.hidden = YES;
            label6_10.hidden = YES;
            labelPlus.hidden = YES;
            break;
        case 7:
            popup7.alpha = 0;
            //popup6.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 8:
            popup8.alpha = 0;
            //popup6.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 9:
            popup9.alpha = 0;
            //popup6.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 10:
            popup10.alpha = 0;
            //popup6.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 11:
            popup11.alpha = 0;
            //popup6.frame = CGRectMake (55, 35, 0, 0);
            break;
            
    }
    [UIView commitAnimations];
}

-(IBAction)showPopUp:(id)sender{
    //NSLog(@"%@", [sender currentTitle]);
    [self animatePopUpShow: [sender currentTitle]];
    
}
-(IBAction)hidePopUp:(id)sender{
    [self animatePopUpHide: [sender tag]];
    
}
-(IBAction)BackHome:(id)sender{
    HomeController *view = [[HomeController alloc] initWithNibName:@"HomeController" bundle:nil];
    view.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:view animated:YES completion:nil];
    
}
- (void) animateLeftShow {
    //    [self addChildViewController:leftController];
    MenuLeftViewController *leftController = [[MenuLeftViewController alloc] init];
    leftController.view.frame = self.leftMenu.bounds;
    [self.leftMenu addSubview:leftController.view];
    [leftController didMoveToParentViewController:self];
    
    [self addChildViewController:leftController];
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    
    leftMenu.alpha = 1;
    bgOpacity.hidden = NO;
    // leftMenu.frame = CGRectMake (0, 0, 342, 768);
    
    leftShown = YES;
    
    [UIView commitAnimations];
}
- (void)animateLeftHide  {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    
    leftMenu.alpha = 0;
    bgOpacity.hidden = YES;
    
    // leftMenu.frame = CGRectMake (0, 0, 342, 768);
    leftShown = NO;
    [UIView commitAnimations];
}
- (void) animateRightShow {
    NSUserDefaults *__autoreleasing defaults = [NSUserDefaults standardUserDefaults];
    NSString  * __autoreleasing current_group = [defaults objectForKey:@"group"];
    
    if([current_group isEqualToString:@"1"]){
        MenuRightViewControllerG1 *rightController = [[MenuRightViewControllerG1 alloc] init];
        rightController.view.frame = self.rightMenu.bounds;
        
        //self.rightMenu = rightController.view;
        [self.rightMenu addSubview:rightController.view];
        [rightController didMoveToParentViewController:self];
        
        [self addChildViewController:rightController];
        
    }
    if([current_group isEqualToString:@"2"]){
        MenuRightViewControllerG2 *rightController = [[MenuRightViewControllerG2 alloc] init];
        rightController.view.frame = self.rightMenu.bounds;
        
        //self.rightMenu = rightController.view;
        [self.rightMenu addSubview:rightController.view];
        [rightController didMoveToParentViewController:self];
        
        [self addChildViewController:rightController];
        
    }
    if([current_group isEqualToString:@"3"]){
        MenuRightViewControllerG3 *rightController = [[MenuRightViewControllerG3 alloc] init];
        rightController.view.frame = self.rightMenu.bounds;
        
        //self.rightMenu = rightController.view;
        [self.rightMenu addSubview:rightController.view];
        [rightController didMoveToParentViewController:self];
        
        [self addChildViewController:rightController];
        
    }
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    
    rightMenu.alpha = 1;
    bgOpacity.hidden = NO;
    //self.rightMenu.frame = CGRectMake (679, 0, 0, 768);
    
    rightShown = YES;
    
    [UIView commitAnimations];
}
- (void)animateRightHide  {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    
    rightMenu.alpha = 0;
    bgOpacity.hidden = YES;
    
    //rightMenu.frame = CGRectMake (679, 0, 0, 768);
    rightShown = NO;
    [UIView commitAnimations];
}


-(IBAction)MenuRight:(id)sender{
    [self animateRightShow];
    rightShown = YES;
}
-(IBAction)MenuLeft:(id)sender{
    NSLog(@"click here left");
    
    [self animateLeftShow];
    leftShown = YES;
    
}
- (void)beginAnimationTick {
    imageView6_1.hidden = NO;
    label6_1.hidden = NO;
    [self performSelector:@selector(beginAnimation1) withObject:nil afterDelay:0.5];

}

- (void)beginAnimation1 {
    labelPlus.hidden = NO;
    [self performSelector:@selector(beginAnimation2) withObject:nil afterDelay:0.5];
    
}
- (void)beginAnimation2 {
    imageView6_2.hidden = NO;
    [self performSelector:@selector(beginAnimation3) withObject:nil afterDelay:0.5];
    
}
- (void)beginAnimation3 {
    imageView6_3.hidden = NO;
    [self performSelector:@selector(beginAnimation4) withObject:nil afterDelay:0.5];
    
}
- (void)beginAnimation4 {
    imageView6_4.hidden= NO;
    label6_4.hidden = NO;
    imageView6_5.hidden = NO;
    label6_5.hidden = NO;
    imageView6_6.hidden = NO;
    label6_6.hidden = NO;
    [self performSelector:@selector(beginAnimation5) withObject:nil afterDelay:0.5];
    
}
- (void)beginAnimation5 {
    imageView6_7.hidden= NO;
    imageView6_8.hidden=NO;
    label6_8.hidden = NO;
    [self performSelector:@selector(beginAnimation6) withObject:nil afterDelay:0.5];
    
}
- (void)beginAnimation6 {
    imageView6_9.hidden= NO;
    imageView6_10.hidden = NO;
    label6_10.hidden = NO;
    
}


@end

//
//  RegisterController.m
//  RenaultTday
//
//  Created by Karim Jebali on 06/11/13.
//
//

#import "RegisterController.h"
#import "XMLReader.h"
#import "LoginController.h"
@interface RegisterController ()

@end

@implementation RegisterController
@synthesize mail, nom, prenom,groupe, password;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backLogin:(id)sender {
    
    LoginController *loginView = [[LoginController alloc] initWithNibName:@"LoginController" bundle:nil];
    loginView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:loginView animated:YES completion:nil];
}
- (IBAction)register:(id)sender {
    @try {
        
        if([[mail text] isEqualToString:@""] || [[password text] isEqualToString:@""] || [[groupe text] isEqualToString:@""] || [[nom text] isEqualToString:@""] || [[prenom text] isEqualToString:@""]) {
            [self alertStatus:@"Tous les champs sont obligatoires" :@"Echec"];
        } else {
            NSString *post =[[NSString alloc] initWithFormat:@"mail=%@&pass=%@&nom=%@&prenom=%@&group=%@&salle=",[mail text],[password text],[nom text],[prenom text],[groupe text]];
            NSLog(@"PostData: %@",post);
            
            NSURL *url=[NSURL URLWithString:@"http://www.kpmc-dev.com/kpmc-apps/inscription.php"];
            
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[url host]];
            
            NSError *error = [[NSError alloc] init];
            NSHTTPURLResponse *response = nil;
            NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            NSLog(@"Response code: %d", [response statusCode]);
            if ([response statusCode] >=200 && [response statusCode] <300)
            {
                NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                NSLog(@"Response ==> %@", responseData);
                if ([responseData isEqualToString: @"error"]) {
                    [self alertStatus:@"Erreur" :@"Echec de la connexion"];
                    
                }else{
                    // Parse the XML into a dictionary
                                        
                    LoginController *homeView = [[LoginController alloc] initWithNibName:@"LoginController" bundle:nil];
                    homeView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    [self presentViewController:homeView animated:YES completion:nil];
                }
            }else {
                if (error) NSLog(@"Error: %@", error);
                [self alertStatus:@"Erreur" :@"Echec de la connexion"];
            }
            
           
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        [self alertStatus:@"inscription échoué" :@"inscription échoué!"];
    }
}
- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    
    [alertView show];
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}

@end

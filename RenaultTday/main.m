//
//  main.m
//  RenaultTday
//
//  Created by MacBook Pro on 01/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"


int main(int argc, char *argv[])
{
    @autoreleasepool {
       // NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
        //[defaults setObject:[NSArray arrayWithObjects:@"fr", @"en", nil] forKey:@"AppleLanguages"];
        //[defaults synchronize];
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}

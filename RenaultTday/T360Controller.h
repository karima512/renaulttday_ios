//
//  FVImageSequenceDemoViewController.h
//  FVImageSequenceDemo
//
//  Created by MacBook Pro on 06/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FVImageSequence.h"
@interface T360Controller : UIViewController {
	IBOutlet FVImageSequence *imageSquence;
    IBOutlet UIButton *LeftButton;
    IBOutlet UIButton *RightButton;
    int current;
	int previous;
    BOOL leftShown;
    BOOL rightShown;
}
@property (strong, nonatomic) IBOutlet UILabel *textAteliers;
@property (strong, nonatomic) IBOutlet UILabel *textMenus;
@property (strong, nonatomic) IBOutlet UILabel *textIndex;
@property (strong, nonatomic) IBOutlet UIView *leftMenu;
@property (strong, nonatomic) IBOutlet UIView *rightMenu;
@property (strong, nonatomic) IBOutlet UIImageView *bgOpacity;



- (IBAction) BackHome: (id)sender;
- (IBAction) MenuRight: (id)sender;
- (IBAction) MenuLeft: (id)sender;

@end


//
//  MenuLeftViewController.h
//  RenaultTday
//
//  Created by MacBook Pro on 05/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuLeftViewController : UIViewController{
    
}
@property (nonatomic, strong) IBOutlet UILabel *labeltextT;
@property (nonatomic, strong) IBOutlet UILabel *labeltextCK;
@property (nonatomic, strong) IBOutlet UILabel *labeltextD;
@property (nonatomic, strong) IBOutlet UIImageView *imageHoverT;
@property (nonatomic, strong) IBOutlet UIImageView *imageHoverCK;
@property (nonatomic, strong) IBOutlet UIImageView *imageHoverD;
@property (nonatomic, strong) IBOutlet UIButton *GammeTbutton, *GammeCKbutton, *GammeDbutton;

- (IBAction) GammeT: (id)sender;
- (IBAction) GammeCK: (id)sender;
- (IBAction) GammeD: (id)sender;

@end

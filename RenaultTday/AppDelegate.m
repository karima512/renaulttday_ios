//
//  AppDelegate.m
//  RenaultTday
//
//  Created by MacBook Pro on 01/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import <mach/mach.h>
#import <mach/mach_host.h>

#import "ViewController.h"

//#import "FaceWithMenus.h"
@implementation AppDelegate

@synthesize window = _window;
@synthesize viewController = _viewController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    if ([self connectedToInternet]) {
        NSLog(@"internet found");
    }else{
        NSLog(@"internet Not found");
   
    }
    NSLog(@"memory size %d", [self get_platform_memory_limit]);
    /*
     
     NSString *stringURL = @"http://www.somewhere.com/fr.xml";
     NSURL  *url = [NSURL URLWithString:stringURL];
     NSData *urlData = [NSData dataWithContentsOfURL:url];
     if ( urlData )
     {
     NSArray       *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
     NSString  *documentsDirectory = [paths objectAtIndex:0];
     
     NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"fr.xml"];
     [urlData writeToFile:filePath atomically:YES];
     }

     
     NSString *xmlPath = [[NSBundle mainBundle] pathForResource:@"fr" ofType:@"xml"];
    NSData *xmlData = [NSData dataWithContentsOfFile:xmlPath];
    //NSLog(@"%@", xmlData);
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:xmlData];
    //[xmlParser setDelegate:self];
    //BOOL result = [xmlParser parser];
    [self.parser parser:xmlParser];*/
    
    /*NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachesDirectory = [paths objectAtIndex:0];
    NSLog(@"%@",cachesDirectory);
    NSFileManager *fileMgr = [NSFileManager defaultManager];
    NSArray *fileArray = [fileMgr contentsOfDirectoryAtPath:cachesDirectory error:nil];
    for (NSString *filename in fileArray)  {
        
        [fileMgr removeItemAtPath:[cachesDirectory stringByAppendingPathComponent:filename] error:NULL];
    }*/
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *menu_label_T = NSLocalizedString(@"Renault Trucks T", @"Renault Trucks T");
    NSString *menu_label_CK = NSLocalizedString(@"Renault Trucks CK", @"Renault Trucks CK");
    NSString *menu_label_D = NSLocalizedString(@"Renault Trucks D", @"Renault Trucks D");
    NSString *atelier_label_avant = NSLocalizedString(@"Face Avant", @"Face Avant");
    NSString *atelier_label_arriere = NSLocalizedString(@"Face Arriere", @"Face Arriere");
    NSString *atelier_label_bord = NSLocalizedString(@"Vie à bord", @"Vie à bord");
    NSString *atelier_label_passager = NSLocalizedString(@"Face Passager", @"Face Passager");
    NSString *atelier_label_360 = NSLocalizedString(@"Face 360", @"Face 360");
    NSString *atelier_label_conducteur = NSLocalizedString(@"Face Conducteur", @"Face Conducteur");
    NSString *atelier_label_cockpit = NSLocalizedString(@"Face Cockpit", @"Face Cockpit");

    [defaults setObject:menu_label_T forKey:@"menu_label_T"];
    [defaults setObject:menu_label_CK forKey:@"menu_label_CK"];
    [defaults setObject:menu_label_D forKey:@"menu_label_D"];
    [defaults setObject:atelier_label_avant forKey:@"atelier_label_avant"];
    [defaults setObject:atelier_label_arriere forKey:@"atelier_label_arriere"];
    [defaults setObject:atelier_label_bord forKey:@"atelier_label_bord"];
    [defaults setObject:atelier_label_passager forKey:@"atelier_label_passager"];
    [defaults setObject:atelier_label_360 forKey:@"atelier_label_360"];
    [defaults setObject:atelier_label_conducteur forKey:@"atelier_label_conducteur"];
    [defaults setObject:atelier_label_cockpit forKey:@"atelier_label_cockpit"];
    [defaults synchronize];
    
    
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    self.viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    return YES;
    
}

-(BOOL)connectedToInternet
{
    NSString *URLString = [NSString stringWithContentsOfURL:[NSURL URLWithString:@"http://www.google.com"]];
    return ( URLString != NULL ) ? YES : NO;
}
-(void)checkVersion
{
    // Create your request string with parameter name as defined in PHP file
    NSString *myRequestString = [NSString stringWithFormat:@"title=%@&description=%@&city=%@"];
    
    // Create Data from request
    NSData *myRequestData = [NSData dataWithBytes: [myRequestString UTF8String] length: [myRequestString length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: @"http://www.youardomain.com/phpfilename.php"]];
    // set Request Type
    [request setHTTPMethod: @"POST"];
    // Set content-type
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"content-type"];
    // Set Request Body
    [request setHTTPBody: myRequestData];
    // Now send a request and get Response
    NSData *returnData = [NSURLConnection sendSynchronousRequest: request returningResponse: nil error: nil];
    // Log Response
    NSString *response = [[NSString alloc] initWithBytes:[returnData bytes] length:[returnData length] encoding:NSUTF8StringEncoding];
    NSLog(@"%@",response);
}
- (void)parserDidStartDocument: (NSXMLParser *)parser {
    NSLog(@"found file and started parsing");
}


- (void)applicationWillResignActive:(UIApplication *)application
{
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
     */
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    /*
     Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
     */
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    /*NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *imagesFiles = [fileManager contentsOfDirectoryAtPath:saveDirectory error:error];
    for (NSString *file in imagesFiles) {
        error = nil;
        [fileManager removeItemAtPath:[saveDirectory stringByAppendingPathComponent:file] error:error];
    }
     
     */
    
    
}
- (int) get_platform_memory_limit
{
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);
    
    vm_statistics_data_t vm_stat;
    
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS) {
        return 50000000; // Default: 50 MB (This is a fairly safe value across current iOS devices)
    } else {
        natural_t mem_used = (vm_stat.active_count +
                              vm_stat.inactive_count +
                              vm_stat.wire_count) * pagesize;
        natural_t mem_free = vm_stat.free_count * pagesize;
        natural_t mem_total = mem_used + mem_free;
        
        // Return 10% of total memory (bytes)
        return (int)mem_total / 10;
    }
}
@end

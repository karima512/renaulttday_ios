//
//  TViewController.h
//  RenaultTday
//
//  Created by MacBook Pro on 06/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TViewController : UIViewController
@property (strong, nonatomic) IBOutlet UILabel *textAteliers;
@property (strong, nonatomic) IBOutlet UILabel *textMenus;
@property (strong, nonatomic) IBOutlet UIView<UIScrollViewDelegate> *popup;


//@property (nonatomic,strong) IBOutlet UIImageView *imageView;
@property (nonatomic,strong) UIView *left;
@property (nonatomic,strong) UIView *right;

@property (strong, nonatomic) IBOutlet UIScrollView *pageScroll;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

- (IBAction) showPopUp: (id)sender;
- (IBAction) hidePopUp: (id)sender;
- (IBAction) BackHome: (id)sender;
- (IBAction) T360: (id)sender;
@end

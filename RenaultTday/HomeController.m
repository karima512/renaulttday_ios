//
//  ViewController.m
//  RenaultTday
//
//  Created by MacBook Pro on 01/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "HomeController.h"
#import "TavantController.h"
#import "TcockController.h"
#import "TarriereController.h"
#import "CKavantController.h"
#import "CKcockController.h"
#import "CKarriereController.h"
#import "DavantController.h"
#import "DcockBordController.h"
#import "DarriereController.h"
@interface HomeController ()
@end
@implementation HomeController




- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    NSLog(@"full memory");
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSLog(@"%@",[defaults objectForKey:@"language"]);
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(IBAction)GammeT:(id)sender{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *category = @"T";
    [defaults setObject:category forKey:@"category"];
    [defaults synchronize];
    
    NSString  * __autoreleasing current_group = [defaults objectForKey:@"group"];
    
    if([current_group isEqualToString:@"1"]){
        
        TavantController *Tview = [[TavantController alloc] initWithNibName:@"TavantController" bundle:nil];
        
        Tview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tview;
    }
    if([current_group isEqualToString:@"2"]){
        
        TcockController *Tview = [[TcockController alloc] initWithNibName:@"TcockController" bundle:nil];
        
        Tview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tview;
    }
    if([current_group isEqualToString:@"3"]){
        
        TarriereController *Tview = [[TarriereController alloc] initWithNibName:@"TarriereController" bundle:nil];
        
        Tview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tview;
    }
    
    //[self.parentViewController presentViewController:Tview animated:YES completion:nil];
    
    
}

-(IBAction)GammeCK:(id)sender{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *category = @"CK";
    [defaults setObject:category forKey:@"category"];
    [defaults synchronize];
    NSString  * __autoreleasing current_group = [defaults objectForKey:@"group"];
    
    if([current_group isEqualToString:@"1"]){
        
        CKavantController *CKview = [[CKavantController alloc] initWithNibName:@"CKavantController" bundle:nil];
        CKview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = CKview;
    }
    if([current_group isEqualToString:@"2"]){
        
        CKcockController *CKview = [[CKcockController alloc] initWithNibName:@"CKcockController" bundle:nil];
        CKview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = CKview;
    }
    if([current_group isEqualToString:@"3"]){
        
        CKarriereController *CKview = [[CKarriereController alloc] initWithNibName:@"CKarriereController" bundle:nil];
        CKview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = CKview;
    }
    //[self.parentViewController presentViewController:CKview animated:YES completion:nil];
}

-(IBAction)GammeD:(id)sender{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *category = @"D";
    [defaults setObject:category forKey:@"category"];
    [defaults synchronize];
    NSString  * __autoreleasing current_group = [defaults objectForKey:@"group"];
    
    if([current_group isEqualToString:@"1"]){
        DavantController *Dview = [[DavantController alloc] initWithNibName:@"DavantController" bundle:nil];
        Dview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Dview;
    }
    if([current_group isEqualToString:@"2"]){
        DcockBordController *Dview = [[DcockBordController alloc] initWithNibName:@"DcockBordController" bundle:nil];
        Dview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Dview;
    }
    if([current_group isEqualToString:@"3"]){
        DarriereController *Dview = [[DarriereController alloc] initWithNibName:@"DarriereController" bundle:nil];
        Dview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Dview;
    }
    //[self.parentViewController presentViewController:Dview animated:YES completion:nil];
}


@end

//
//  CKViewController.m
//  RenaultTday
//
//  Created by MacBook Pro on 06/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "CKavantController.h"
#import "ViewController.h"
#import "HomeController.h"
#import "MenuLeftViewController.h"
#import "MenuRightViewControllerG1.h"
#import "MenuRightViewControllerG2.h"
#import "MenuRightViewControllerG3.h"
#import <QuartzCore/QuartzCore.h>

@interface CKavantController()
@end
@implementation CKavantController

@synthesize textAteliers;
@synthesize textMenus, textIndex;
@synthesize popup1,popup2,popup3,popup4,popup5,popup6,popup7,popup8,popup9;
@synthesize imageView1_2_1,imageView1_2_2,imageView1_2_3,imageView4_1, imageView5_1;
@synthesize pageScroll1, pageScroll2,pageScroll3,pageScroll4,pageScroll5,pageScroll6,pageScroll7,pageScroll8,pageScroll9;
@synthesize pageControl1,pageControl2,pageControl3,pageControl4,pageControl5,pageControl6,pageControl7,pageControl8,pageControl9;
@synthesize leftMenu , rightMenu , bgOpacity;

@synthesize btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    NSLog(@"full memory");
    
    // Release any cached data, images, etc that aren't in use.
}
-(void)makeRoundedImage:(UIButton *) button

{
    UIImage * img = [UIImage imageNamed:@"bg_btn_middle.png"];
    [button setBackgroundImage:img forState:UIControlStateNormal];
    
    NSString *titleString = [button currentTitle];  // get button title
    
    CGSize fontSize = [titleString sizeWithFont:[UIFont systemFontOfSize:18.0]];
    
    CGRect currentFrame = button.frame;
    
    CGRect buttonFrame = CGRectMake(currentFrame.origin.x, currentFrame.origin.y, fontSize.width + 30.0, fontSize.height + 12.0);
    button.frame = buttonFrame;
    
    button.clipsToBounds = YES;
    
    button.layer.cornerRadius = 10;
    
    button.layer.borderWidth=1.0f;
    
    [button setTitle:titleString forState: UIControlStateNormal];
    
    [button.titleLabel setFont:[UIFont fontWithName:@"DIN" size:15]];
    
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *menu = @"avant";
    [defaults setObject:menu forKey:@"menu"];
    [defaults synchronize];
    
    
    NSString *cat = @"";
    NSString *current_cat = [defaults objectForKey:@"category"];
    if ([current_cat isEqualToString:@"T" ]) {
        cat= [defaults objectForKey:@"menu_label_T"];
    }else if ([current_cat isEqualToString:@"CK" ]) {
        cat= [defaults objectForKey:@"menu_label_CK"];
    }
    else if ([current_cat isEqualToString:@"D" ]) {
        cat= [defaults objectForKey:@"menu_label_D"];
    }
    textIndex.text = [NSString stringWithFormat:@"%@ / %@",cat,[defaults objectForKey:@"atelier_label_avant"]];
    
    
    self.leftMenu.alpha = 0;
    self.leftMenu.frame = CGRectMake (0, 0, 342, 768);
    
    leftShown = NO;
    
    self.rightMenu.alpha = 0;
    // self.rightMenu.frame = CGRectMake (679, 0, 0, 768);
    rightShown = NO;
    //rightMenu.userInteractionEnabled = YES ;
    
    
    textAteliers.transform = CGAffineTransformMakeRotation(M_PI / 2);
    textMenus.transform = CGAffineTransformMakeRotation(-M_PI / 2);
    popup1.alpha = 0;
    popup2.alpha = 0;
    popup3.alpha = 0;
    popup4.alpha = 0;
    popup5.alpha = 0;
    popup6.alpha = 0;
    popup7.alpha = 0;
    popup8.alpha = 0;
    popup9.alpha = 0;
    
    [self makeRoundedImage:btn1];
    [self makeRoundedImage:btn2];
    [self makeRoundedImage:btn3];
    [self makeRoundedImage:btn4];
    [self makeRoundedImage:btn5];
    [self makeRoundedImage:btn6];
    [self makeRoundedImage:btn7];
    [self makeRoundedImage:btn8];
    [self makeRoundedImage:btn9];
    
    //self.popup.delegate = self.delegate;
    [self.view addSubview:popup1];
    [self.view addSubview:popup2];
    [self.view addSubview:popup3];
    [self.view addSubview:popup4];
    [self.view addSubview:popup5];
    [self.view addSubview:popup6];
    [self.view addSubview:popup7];
    [self.view addSubview:popup8];
    [self.view addSubview:popup9];
    
    
    
    /* UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(200, 200, 14, 14)];
     imgView.image = [UIImage imageNamed:@"btn_slide_circle.png"];
     [popup addSubview:imgView];
     [imgView release];*/
    // Do any additional setup after loading the view from its nib.
}
-(void)viewDidAppear:(BOOL)animated{
    pageScroll1.delegate = self;
    pageControl1.numberOfPages = 2;
    pageControl1.currentPage = 0;
    
    pageScroll2.delegate = self;
    pageControl2.numberOfPages = 3;
    pageControl2.currentPage = 0;
    
    pageScroll3.delegate = self;
    pageControl3.numberOfPages = 1;
    pageControl3.currentPage = 0;
    
    pageScroll4.delegate = self;
    pageControl4.numberOfPages = 1;
    pageControl4.currentPage = 0;
    
    pageScroll5.delegate = self;
    pageControl5.numberOfPages = 1;
    pageControl5.currentPage = 0;
    
    pageScroll6.delegate = self;
    pageControl6.numberOfPages = 1;
    pageControl6.currentPage = 0;
    
    pageScroll7.delegate = self;
    pageControl7.numberOfPages = 1;
    pageControl7.currentPage = 0;
    
    pageScroll8.delegate = self;
    pageControl8.numberOfPages = 1;
    pageControl8.currentPage = 0;

    pageScroll9.delegate = self;
    pageControl9.numberOfPages = 1;
    pageControl9.currentPage = 0;

    pageScroll1.contentSize = CGSizeMake(self.popup1.frame.size.width * pageControl1.numberOfPages, self.popup1.frame.size.height);
    pageScroll2.contentSize = CGSizeMake(self.popup2.frame.size.width * pageControl2.numberOfPages, self.popup2.frame.size.height);
    pageScroll3.contentSize = CGSizeMake(self.popup3.frame.size.width * pageControl3.numberOfPages, self.popup3.frame.size.height);
    pageScroll4.contentSize = CGSizeMake(self.popup4.frame.size.width * pageControl4.numberOfPages, self.popup4.frame.size.height);
    pageScroll5.contentSize = CGSizeMake(self.popup5.frame.size.width * pageControl5.numberOfPages, self.popup5.frame.size.height);
    pageScroll6.contentSize = CGSizeMake(self.popup6.frame.size.width * pageControl6.numberOfPages, self.popup6.frame.size.height);
    pageScroll7.contentSize = CGSizeMake(self.popup7.frame.size.width * pageControl7.numberOfPages, self.popup7.frame.size.height);
    pageScroll8.contentSize = CGSizeMake(self.popup8.frame.size.width * pageControl8.numberOfPages, self.popup8.frame.size.height);
    pageScroll9.contentSize = CGSizeMake(self.popup9.frame.size.width * pageControl9.numberOfPages, self.popup9.frame.size.height);
    
    
}
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touchLeft = [[event allTouches] anyObject]  ;
    CGPoint touchLocationLeft = [touchLeft locationInView:self.leftMenu];
    NSLog(@"left %@",[NSValue valueWithCGPoint:touchLocationLeft]);
    
    if (![self.leftMenu pointInside:touchLocationLeft withEvent:event] && leftShown ) {
        [self animateLeftHide];
    }
    
    UITouch *touchRight = [[event allTouches] anyObject]  ;
    
    CGPoint touchLocationRight = [touchRight locationInView:self.rightMenu];
    NSLog(@"right %@",[NSValue valueWithCGPoint:touchLocationRight]);
    /* if ([self.rightMenu pointInside: [self.view convertPoint:touchLocationRight toView: rightMenu] withEvent:event]) {
     [self animateRightHide];
     NSLog(@"right 11");
     
     }*/
    
    if (![self.rightMenu pointInside:touchLocationRight withEvent:event] && rightShown ) {
        [self animateRightHide];
        NSLog(@"right 222");
    }
    
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft || interfaceOrientation == UIInterfaceOrientationLandscapeRight)
		return YES;
	return NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.textAteliers = nil;
    self.textIndex = nil;
    self.textMenus = nil;
    self.popup1 = nil;
    self.popup2 = nil;
    self.popup3 = nil;
    self.popup4 = nil;
    self.popup5 = nil;
    self.popup6 = nil;
    self.popup7 = nil;
    self.popup8 = nil;
    self.popup9 = nil;
    self.pageScroll1 = nil;
    self.pageScroll2 = nil;
    self.pageScroll3 = nil;
    self.pageScroll4 = nil;
    self.pageScroll5 = nil;
    self.pageScroll6 = nil;
    self.pageScroll7 = nil;
    self.pageScroll8 = nil;
    self.pageScroll9 = nil;

}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth1 = self.popup1.frame.size.width;
    int page = floor((pageScroll1.contentOffset.x - pageWidth1 / pageControl1.numberOfPages) / pageWidth1) + 1;
    NSLog(@"page ; %d",page);
    pageControl1.currentPage = page;
    switch (pageControl1.currentPage) {
        case 0:
            break;
        case 1:
            //imageView1_2_1.transform = CGAffineTransformMakeScale(0.5f, 0.5f);
            [UIView animateWithDuration:1.5
                                  delay:0
                                options:UIViewAnimationOptionBeginFromCurrentState
                             animations:(void (^)(void)) ^{
                                 imageView1_2_1.transform=CGAffineTransformMakeScale(1.5, 1.5);
                             }
                             completion:^(BOOL finished){
                                 //imageView1_2_1.transform=CGAffineTransformIdentity;
                             }];
            imageView1_2_1.transform = CGAffineTransformMakeScale(1.5, 1.5);
            //[NSThread sleepForTimeInterval:.5];
            //imageView1_2_2.transform = CGAffineTransformMakeScale(0.5f, 0.5f);
            [UIView animateWithDuration:1.5
                                  delay:1.5
                                options:UIViewAnimationOptionBeginFromCurrentState
                             animations:(void (^)(void)) ^{
                                 imageView1_2_2.transform=CGAffineTransformMakeScale(1.5, 1.5);
                             }
                             completion:^(BOOL finished){
                                 //imageView1_2_2.transform=CGAffineTransformIdentity;
                             }];
            imageView1_2_2.transform = CGAffineTransformMakeScale(1.5, 1.5);
            //[NSThread sleepForTimeInterval:.5];
            //imageView1_2_3.transform = CGAffineTransformMakeScale(0.5f, 0.5f);
            [UIView animateWithDuration:1.5
                                  delay:3
                                options:UIViewAnimationOptionBeginFromCurrentState
                             animations:(void (^)(void)) ^{
                                 imageView1_2_3.transform=CGAffineTransformMakeScale(1.5, 1.5);
                             }
                             completion:^(BOOL finished){
                                 //imageView1_2_3.transform=CGAffineTransformIdentity;
                             }];
            imageView1_2_3.transform = CGAffineTransformMakeScale(1.5, 1.5);

            break;
    }
    CGFloat pageWidth2 = self.popup2.frame.size.width;
    int page2 = floor((pageScroll2.contentOffset.x - pageWidth2 / pageControl2.numberOfPages) / pageWidth2) + 1;
    NSLog(@"page ; %d",page2);
    pageControl2.currentPage = page2;
    
    CGFloat pageWidth3 = self.popup3.frame.size.width;
    int page3 = floor((pageScroll3.contentOffset.x - pageWidth3 / pageControl3.numberOfPages) / pageWidth3) + 1;
    NSLog(@"page ; %d",page2);
    pageControl3.currentPage = page3;
    
    CGFloat pageWidth4 = self.popup4.frame.size.width;
    int page4 = floor((pageScroll4.contentOffset.x - pageWidth4 / pageControl4.numberOfPages) / pageWidth4) + 1;
    NSLog(@"page ; %d",page4);
    pageControl4.currentPage = page4;
    
    CGFloat pageWidth5 = self.popup5.frame.size.width;
    int page5 = floor((pageScroll5.contentOffset.x - pageWidth5 / pageControl5.numberOfPages) / pageWidth5) + 1;
    NSLog(@"page ; %d",page5);
    pageControl5.currentPage = page5;
    
    CGFloat pageWidth6 = self.popup6.frame.size.width;
    int page6 = floor((pageScroll6.contentOffset.x - pageWidth6 / pageControl6.numberOfPages) / pageWidth6) + 1;
    NSLog(@"page ; %d",page6);
    pageControl6.currentPage = page6;
    
    CGFloat pageWidth7 = self.popup7.frame.size.width;
    int page7 = floor((pageScroll7.contentOffset.x - pageWidth7 / pageControl7.numberOfPages) / pageWidth7) + 1;
    NSLog(@"page ; %d",page7);
    pageControl7.currentPage = page7;
    
    CGFloat pageWidth8 = self.popup8.frame.size.width;
    int page8 = floor((pageScroll8.contentOffset.x - pageWidth8 / pageControl8.numberOfPages) / pageWidth8) + 1;
    NSLog(@"page ; %d",page8);
    pageControl8.currentPage = page8;
    
    CGFloat pageWidth9 = self.popup9.frame.size.width;
    int page9 = floor((pageScroll9.contentOffset.x - pageWidth9 / pageControl9.numberOfPages) / pageWidth9) + 1;
    NSLog(@"page ; %d",page9);
    pageControl9.currentPage = page9;
    
}


/*
 -(void) updateDots {
 for (int i = 0; i < [self.pageScroll.subviews count]; i++)
 {
 UIImageView* dot = [self.pageScroll.subviews objectAtIndex:i];
 
 if (i == self.pageControl.currentPage) {
 
 dot.image = [UIImage imageNamed:@"btn_slide_circle_selected.png"];
 
 
 } else {
 dot.image = [UIImage imageNamed:@"btn_slide_circle.png"];
 }
 }
 }
 */
- (void) initPopUpView {
}

- (void) animatePopUpShow:(NSString *)popid {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(initPopUpView)];
    NSLog(@"%d",[popid intValue]);
    switch ([popid intValue]) {
        case 1:
            // Do some think here
            popup1.alpha = 1;
            popup1.frame = CGRectMake (55, 35, 950, 685);
            
            
            break;
        case 2:
            // Do some think here
            popup2.alpha = 1;
            popup2.frame = CGRectMake (55, 35, 950, 685);
            
            break;
        case 3:
            // Do some think here
            popup3.alpha = 1;
            popup3.frame = CGRectMake (55, 35, 950, 685);
            
            break;
        case 4:
            // Do some think here
            popup4.alpha = 1;
            popup4.frame = CGRectMake (55, 35, 950, 685);
            
            animationArray=[NSArray arrayWithObjects:
                            [UIImage imageNamed:@"P015238.png"],
                            [UIImage imageNamed:@"P015237.png"],
                            nil];
            imageView4_1.animationImages=animationArray;
            imageView4_1.animationDuration=2;
            imageView4_1.animationRepeatCount=0;
            imageView4_1.image = [UIImage imageNamed:@"P015237.png"];
            [imageView4_1 startAnimating];
                        
            
            break;
        case 5:
            // Do some think here
            popup5.alpha = 1;
            popup5.frame = CGRectMake (55, 35, 950, 685);
            animationArray=[NSArray arrayWithObjects:
                            [UIImage imageNamed:@"P019700.png"],
                            [UIImage imageNamed:@"P019699.png"],
                            [UIImage imageNamed:@"P019698.png"],
                            [UIImage imageNamed:@"P019697.png"],
                            [UIImage imageNamed:@"P019696.png"],
                            [UIImage imageNamed:@"P019695.png"],
                            nil];
            imageView5_1.animationImages=animationArray;
            imageView5_1.animationDuration=6;
            imageView5_1.animationRepeatCount=0;
            imageView5_1.image = [UIImage imageNamed:@"P019695.png"];
            [imageView5_1 startAnimating];
            
            
            break;
        case 6:
            // Do some think here
            popup6.alpha = 1;
            popup6.frame = CGRectMake (55, 35, 950, 685);
            
            break;
        case 7:
            // Do some think here
            popup7.alpha = 1;
            popup7.frame = CGRectMake (55, 35, 950, 685);
            
            break;
        case 8:
            // Do some think here
            popup8.alpha = 1;
            popup8.frame = CGRectMake (55, 35, 950, 685);
            
            break;
        case 9:
            // Do some think here
            popup9.alpha = 1;
            popup9.frame = CGRectMake (55, 35, 950, 685);
            
            break;
        default:
            NSLog(@"Default Message here");
            break;
    }
    //float popUpY = popup.frame.size.height;
    //float popUpX = popup.frame.size.width;
    //float viewY = self.view.frame.size.height;
    //float viewX = self.view.frame.size.width;
    
    [UIView commitAnimations];
}
- (void) animatePopUpHide:(int)popid {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    NSLog(@"hide: %d",popid);
    switch (popid) {
        case 1:
            popup1.alpha = 0;
            //popup1.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 2:
            popup2.alpha = 0;
            //popup2.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 3:
            popup3.alpha = 0;
            //popup3.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 4:
            popup4.alpha = 0;
            //popup4.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 5:
            popup5.alpha = 0;
            //popup5.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 6:
            popup6.alpha = 0;
            //popup6.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 7:
            popup7.alpha = 0;
            //popup7.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 8:
            popup8.alpha = 0;
            //popup8.frame = CGRectMake (55, 35, 0, 0);
            break;
        case 9:
            popup9.alpha = 0;
            //popup8.frame = CGRectMake (55, 35, 0, 0);
            break;
            
    }
    [UIView commitAnimations];
}

-(IBAction)showPopUp:(id)sender{
    LeftButton.enabled = NO;
    RightButton.enabled = NO;

    //NSLog(@"%@", [sender currentTitle]);
    [self animatePopUpShow: [sender currentTitle]];
    
}
-(IBAction)hidePopUp:(id)sender{
    LeftButton.enabled = YES;
    RightButton.enabled = YES;

    [self animatePopUpHide: [sender tag]];
    
}
-(IBAction)BackHome:(id)sender{
    HomeController *view = [[HomeController alloc] initWithNibName:@"HomeController" bundle:nil];
    view.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:view animated:YES completion:nil];
    
}
- (void) animateLeftShow {
    //    [self addChildViewController:leftController];
    MenuLeftViewController *leftController = [[MenuLeftViewController alloc] init];
    leftController.view.frame = self.leftMenu.bounds;
    [self.leftMenu addSubview:leftController.view];
    [leftController didMoveToParentViewController:self];
    
    [self addChildViewController:leftController];
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    
    leftMenu.alpha = 1;
    bgOpacity.hidden = NO;
    // leftMenu.frame = CGRectMake (0, 0, 342, 768);
    
    leftShown = YES;
    
    [UIView commitAnimations];
}
- (void)animateLeftHide  {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    
    leftMenu.alpha = 0;
    bgOpacity.hidden = YES;
    
    // leftMenu.frame = CGRectMake (0, 0, 342, 768);
    leftShown = NO;
    [UIView commitAnimations];
}
- (void) animateRightShow {
    NSUserDefaults *__autoreleasing defaults = [NSUserDefaults standardUserDefaults];
    NSString  * __autoreleasing current_group = [defaults objectForKey:@"group"];
    
    if([current_group isEqualToString:@"1"]){
        MenuRightViewControllerG1 *rightController = [[MenuRightViewControllerG1 alloc] init];
        rightController.view.frame = self.rightMenu.bounds;
        
        //self.rightMenu = rightController.view;
        [self.rightMenu addSubview:rightController.view];
        [rightController didMoveToParentViewController:self];
        
        [self addChildViewController:rightController];
        
    }
    if([current_group isEqualToString:@"2"]){
        MenuRightViewControllerG2 *rightController = [[MenuRightViewControllerG2 alloc] init];
        rightController.view.frame = self.rightMenu.bounds;
        
        //self.rightMenu = rightController.view;
        [self.rightMenu addSubview:rightController.view];
        [rightController didMoveToParentViewController:self];
        
        [self addChildViewController:rightController];
        
    }
    if([current_group isEqualToString:@"3"]){
        MenuRightViewControllerG3 *rightController = [[MenuRightViewControllerG3 alloc] init];
        rightController.view.frame = self.rightMenu.bounds;
        
        //self.rightMenu = rightController.view;
        [self.rightMenu addSubview:rightController.view];
        [rightController didMoveToParentViewController:self];
        
        [self addChildViewController:rightController];
        
    }
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    
    rightMenu.alpha = 1;
    bgOpacity.hidden = NO;
    //self.rightMenu.frame = CGRectMake (679, 0, 0, 768);
    
    rightShown = YES;
    
    [UIView commitAnimations];
}
- (void)animateRightHide  {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.1];
    [UIView setAnimationDelegate:self];
    
    rightMenu.alpha = 0;
    bgOpacity.hidden = YES;
    
    //rightMenu.frame = CGRectMake (679, 0, 0, 768);
    rightShown = NO;
    [UIView commitAnimations];
}


-(IBAction)MenuRight:(id)sender{
    [self animateRightShow];
    rightShown = YES;
}
-(IBAction)MenuLeft:(id)sender{
    NSLog(@"click here left");
    
    [self animateLeftShow];
    leftShown = YES;
    
}



@end

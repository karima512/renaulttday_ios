//
//  MenuRightViewController.h
//  RenaultTday
//
//  Created by MacBook Pro on 05/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuRightViewControllerG3 : UIViewController{
    
}
@property (nonatomic, strong) IBOutlet UILabel *labeltext1;
@property (nonatomic, strong) IBOutlet UILabel *labeltext2;
@property (nonatomic, strong) IBOutlet UILabel *labeltext3;
@property (nonatomic, strong) IBOutlet UILabel *labeltext4;
@property (nonatomic, strong) IBOutlet UILabel *labeltext5;
@property (nonatomic, strong) IBOutlet UILabel *labeltext6;
@property (nonatomic, strong) IBOutlet UILabel *labeltext7;

@property (nonatomic, strong) IBOutlet UILabel *labeldestext1;
@property (nonatomic, strong) IBOutlet UILabel *labeldestext2;
@property (nonatomic, strong) IBOutlet UILabel *labeldestext3;
@property (nonatomic, strong) IBOutlet UILabel *labeldestext4;
@property (nonatomic, strong) IBOutlet UILabel *labeldestext5;
@property (nonatomic, strong) IBOutlet UILabel *labeldestext6;
@property (nonatomic, strong) IBOutlet UILabel *labeldestext7;

@property (nonatomic, strong) IBOutlet UIImageView *imageHover1;
@property (nonatomic, strong) IBOutlet UIImageView *imageHover2;
@property (nonatomic, strong) IBOutlet UIImageView *imageHover3;
@property (nonatomic, strong) IBOutlet UIImageView *imageHover4;
@property (nonatomic, strong) IBOutlet UIImageView *imageHover5;
@property (nonatomic, strong) IBOutlet UIImageView *imageHover6;
@property (nonatomic, strong) IBOutlet UIImageView *imageHover7;

@property (nonatomic, strong) IBOutlet UIImageView *image1;
@property (nonatomic, strong) IBOutlet UIImageView *image2;
@property (nonatomic, strong) IBOutlet UIImageView *image3;
@property (nonatomic, strong) IBOutlet UIImageView *image4;
@property (nonatomic, strong) IBOutlet UIImageView *image5;
@property (nonatomic, strong) IBOutlet UIImageView *image6;
@property (nonatomic, strong) IBOutlet UIImageView *image7;
@property (nonatomic, strong) IBOutlet UIButton *button1, *button2, *button3, *button4,*button5, *button6, *button7;

- (IBAction) goToAvant: (id)sender;
- (IBAction) goToArriere: (id)sender;
- (IBAction) goToBord: (id)sender;
- (IBAction) goTo360: (id)sender;
- (IBAction) goToCock: (id)sender;
- (IBAction) goToPassager: (id)sender;
- (IBAction) goToConducteur: (id)sender;

@end

//
//  GroupController.m
//  RenaultTday
//
//  Created by Karim Jebali on 02/12/13.
//
//

#import "GroupController.h"
#import "HomeController.h"
@interface GroupController ()

@end

@implementation GroupController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(IBAction)group1:(id)sender{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"1" forKey:@"group"];
    [defaults synchronize];

    HomeController *homeView = [[HomeController alloc] initWithNibName:@"HomeController" bundle:nil];
    homeView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:homeView animated:YES completion:nil];
}
-(IBAction)group2:(id)sender{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"2" forKey:@"group"];
    [defaults synchronize];
    
    HomeController *homeView = [[HomeController alloc] initWithNibName:@"HomeController" bundle:nil];
    homeView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:homeView animated:YES completion:nil];
}
-(IBAction)group3:(id)sender{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:@"3" forKey:@"group"];
    [defaults synchronize];
    
    HomeController *homeView = [[HomeController alloc] initWithNibName:@"HomeController" bundle:nil];
    homeView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:homeView animated:YES completion:nil];
}
@end

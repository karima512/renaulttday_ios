//
//  LoginController.m
//  RenaultTday
//
//  Created by Karim Jebali on 05/11/13.
//
//

#import "LoginController.h"
#import "HomeController.h"
#import "XMLReader.h"
#import "RegisterController.h"
@interface LoginController ()

@end

@implementation LoginController
@synthesize login, password;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)connect:(id)sender {
    @try {
        
        if([[login text] isEqualToString:@""] || [[password text] isEqualToString:@""] ) {
            [self alertStatus:@"Entrer login et mot de passe" :@"Echec de la connexion"];
        } else {
            NSString *post =[[NSString alloc] initWithFormat:@"login=%@&pass=%@",[login text],[password text]];
            NSLog(@"PostData: %@",post);
            
            NSURL *url=[NSURL URLWithString:@"http://www.kpmc-dev.com/kpmc-apps/authentification.php"];
            
            NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
            
            NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
            
            NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
            [request setURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
            [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
            [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
            [request setHTTPBody:postData];
            
            //[NSURLRequest setAllowsAnyHTTPSCertificate:YES forHost:[url host]];
            
            NSError *error = [[NSError alloc] init];
            NSHTTPURLResponse *response = nil;
            NSData *urlData=[NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
            
            NSLog(@"Response code: %d", [response statusCode]);
            if ([response statusCode] >=200 && [response statusCode] <300)
            {
                NSString *responseData = [[NSString alloc]initWithData:urlData encoding:NSUTF8StringEncoding];
                NSLog(@"Response ==> %@", responseData);
                if ([responseData isEqualToString: @"impossible"] || [responseData isEqualToString: @"error"]) {
                    [self alertStatus:@"Erreur" :@"Echec de la connexion"];
                    
                }else{
                // Parse the XML into a dictionary
                NSError *parseError = nil;
                NSDictionary *xmlDictionary = [XMLReader dictionaryForXMLString:responseData error:&parseError];
                
                // Print the dictionary
                NSLog(@"%@", xmlDictionary);
                    int group = [[[[xmlDictionary objectForKey:@"users"] objectForKey:@"user"]objectForKey:@"group"]objectForKey:@"text"];
                NSLog(@"group_id: %@",[[[[xmlDictionary objectForKey:@"users"] objectForKey:@"user"]objectForKey:@"group"]objectForKey:@"text"] );
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setInteger:group forKey:@"group"];
                NSString *user = [login text];
                [defaults setObject:user forKey:@"user"];
                [defaults synchronize];
                    
                HomeController *homeView = [[HomeController alloc] initWithNibName:@"HomeController" bundle:nil];
                    homeView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
                    [self presentViewController:homeView animated:YES completion:nil];
                }
            }else {
                if (error) NSLog(@"Error: %@", error);
                [self alertStatus:@"Erreur" :@"Echec de la connexion"];
            }
            
                /*SBJsonParser *jsonParser = [SBJsonParser new];
                NSDictionary *jsonData = (NSDictionary *) [jsonParser objectWithString:responseData error:nil];
                NSLog(@"%@",jsonData);
                NSInteger success = [(NSNumber *) [jsonData objectForKey:@"success"] integerValue];
                NSLog(@"%d",success);
                if(success == 1)
                {
                    NSLog(@"Login SUCCESS");
                    [self alertStatus:@"Logged in Successfully." :@"Login Success!"];
                    
                } else {
                    
                    NSString *error_msg = (NSString *) [jsonData objectForKey:@"error_message"];
                    [self alertStatus:error_msg :@"Login Failed!"];
                }
                
            } else {
                if (error) NSLog(@"Error: %@", error);
                [self alertStatus:@"Connection Failed" :@"Login Failed!"];
            }*/
        }
    }
    @catch (NSException * e) {
        NSLog(@"Exception: %@", e);
        [self alertStatus:@"Login Failed." :@"Login Failed!"];
    }
}
- (void) alertStatus:(NSString *)msg :(NSString *)title
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:msg
                                                       delegate:self
                                              cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil, nil];
    
    [alertView show];
}
-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = YES; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}";
    NSString *laxString = @".+@([A-Za-z0-9]+\\.)+[A-Za-z]{2}[A-Za-z]*";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
-(IBAction)goToregister:(id)sender{
    RegisterController *homeView = [[RegisterController alloc] initWithNibName:@"RegisterController" bundle:nil];
    homeView.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:homeView animated:YES completion:nil];
}
@end

//
//  LoginController.h
//  RenaultTday
//
//  Created by Karim Jebali on 05/11/13.
//
//

#import <UIKit/UIKit.h>

@interface LoginController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *login;
@property (strong, nonatomic) IBOutlet UITextField *password;

-(IBAction)connect:(id)sender;
-(IBAction)goToregister:(id)sender;

@end

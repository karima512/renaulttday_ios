//
//  ViewController.m
//  RenaultTday
//
//  Created by MacBook Pro on 01/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"
#import "HomeController.h"
#import "MenuLeftViewController.h"
#import "GroupController.h"
#import "LoginController.h"
#import "LocalizationSystem.h"
@interface ViewController ()
@end
@implementation ViewController
@synthesize currentElement = _currentElement;
@synthesize logout;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *user_connected = [defaults objectForKey:@"user"];
    NSLog(@"%@",user_connected);
    if(user_connected != nil){
        [logout setHidden:NO];
    }
	// Do any additional setup after loading the view, typically from a nib.
    // path to local xml document
    /*NSURL *path = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"fr" ofType:@"xml"]];
	
    NSXMLParser *parser = [[NSXMLParser alloc]initWithContentsOfURL:path];
    
    [parser setDelegate:self];
    
    // begin parsing document
    [parser parse];*/
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(IBAction)frenshClick:(id)sender{

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *language = @"fr";
    [defaults setInteger:12 forKey:@"id"];
    [defaults setObject:language forKey:@"language"];
    [defaults removeObjectForKey:@"AppleLanguages"];
    //[[NSUserDefaults standardUserDefaults] setPersistentDomain:[NSArray arrayWithObjects:@"fr",@"en", nil] forName:@"AppleLanguages"];

    [defaults setObject:[NSArray arrayWithObjects:@"fr",@"en", nil] forKey:@"AppleLanguages"];
    NSString *user_connected = [defaults objectForKey:@"user"];
    [[NSUserDefaults standardUserDefaults] setObject:language forKey:@"ICPreferredLanguage"];
    [[NSUserDefaults standardUserDefaults] synchronize];

    [defaults synchronize];
    //LocalizationSetLanguage(@"fr");
    


    
    //[[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"fr", nil] forKey:@"AppleLanguages"];
   // [[NSUserDefaults standardUserDefaults] synchronize]; //to make the change immediate
    //if (user_connected == nil){
    if(false){
    LoginController *homeView = [[LoginController alloc] initWithNibName:@"LoginController" bundle:nil];
        homeView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:homeView animated:YES completion:nil];

    }else{
        GroupController *homeView = [[GroupController alloc] initWithNibName:@"GroupController" bundle:nil];
        homeView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:homeView animated:YES completion:nil];


    }
    
    
    
}
- (IBAction)englishClick:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *language = @"en";
    //NSLog(@"%@",language);
    [defaults setObject:language forKey:@"language"];
    //NSLog(@"%@",[defaults objectForKey:@"language"]);
    
    NSString *user_connected = [defaults objectForKey:@"user"];
    [defaults removeObjectForKey:@"AppleLanguages"];
    [defaults setObject:[NSArray arrayWithObjects:@"en",@"fr", nil] forKey:@"AppleLanguages"];
    [defaults synchronize];
    
    /*
    NSString *appDomain = [[NSBundle mainBundle] bundleIdentifier];
    [[NSUserDefaults standardUserDefaults] removePersistentDomainForName:appDomain];
    */
    
    

    //NSLog(@"synchro %hhd", [defaults synchronize]);
    //LocalizationSetLanguage(@"en");
    
    // [[NSUserDefaults standardUserDefaults] setObject:[NSArray arrayWithObjects:@"en", nil] forKey:@"AppleLanguages"];
   // [[NSUserDefaults standardUserDefaults] synchronize]; //to make the change immediate
    //if (user_connected == nil){
    if(false){
        LoginController *homeView = [[LoginController alloc] initWithNibName:@"LoginController" bundle:nil];
        homeView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:homeView animated:YES completion:nil];
        
    }else{
        GroupController *homeView = [[GroupController alloc] initWithNibName:@"GroupController" bundle:nil];
        homeView.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self presentViewController:homeView animated:YES completion:nil];
        
    }

    
}
-(void) parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    if ([elementName isEqualToString:@"group"]) {
        int group = [[attributeDict objectForKey:@"id"] integerValue];
        
        NSLog(@"Reading id value :%i",group);
        NSLog(@" ");     // newline
    }
    NSLog(@"Found element: <%@>", elementName);
}


-(void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if (self.currentElement == nil) {
        self.currentElement = [[NSMutableString alloc] init];
    }
    self.currentElement = [NSMutableString stringWithFormat:@"%@", string];
}


-(void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    if ([elementName isEqualToString:@"group"]) 
    {
        NSLog(@"Found end: </%@>", elementName);
        return;
    }
    else if ([elementName isEqualToString:@"group1"]) {
        NSLog(@"Found Data: %@", self.currentElement);
        return;
    } 
    else if ([elementName isEqualToString:@"group2"]) {
        return;
    } else {
        NSLog(@"Found Data: %@", self.currentElement);
    }
    NSLog(@"Found end: </%@>", elementName);
}
-(IBAction)deconnect:(id)sender
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Deconnexion"
                                                        message:@"Vous allez se déconnecter"
                                                       delegate:self
                                              cancelButtonTitle:@"Annuler"
                                              otherButtonTitles:@"Deconnexion", nil];

    [alertView setTag:12];
    [alertView show];
}
- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if ([alertView tag] == 12) {    // it's the Error alert
        if (buttonIndex == 0) {     // and they clicked OK.
            NSLog(@" annuler");
        }
        if (buttonIndex == 1) {     // and they clicked OK.
            NSLog(@" Deconnexion");
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:nil forKey:@"user"];
            [defaults synchronize];
            [logout setHidden:YES];
        }
    }
}
@end



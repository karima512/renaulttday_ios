//
//  TViewController.m
//  RenaultTday
//
//  Created by MacBook Pro on 06/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "TViewController.h"
#import "ViewController.h"
#import "T360Controller.h"

@implementation TViewController
@synthesize textAteliers;
@synthesize textMenus;
@synthesize popup;
@synthesize left = _left;
@synthesize right = _right;
@synthesize pageScroll;
@synthesize pageControl;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    textAteliers.transform = CGAffineTransformMakeRotation(M_PI / 2);    
    textMenus.transform = CGAffineTransformMakeRotation(-M_PI / 2);
    popup.alpha = 0;
    //self.popup.delegate = self.delegate;
    [self.view addSubview:popup];
    pageControl.numberOfPages = 2;
    pageControl.currentPage = 0;
    //pageScroll.delegate = self.popup;
    
    pageScroll.contentSize = CGSizeMake(self.popup.frame.size.width * 2, self.popup.frame.size.height);
   
        /* UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(200, 200, 14, 14)];
    imgView.image = [UIImage imageNamed:@"btn_slide_circle.png"];
    [popup addSubview:imgView];
    [imgView release];*/
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView
{	
    CGFloat pageWidth = self.popup.frame.size.width;
    int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    NSLog(@"page ; %d",page);
    pageControl.currentPage = page;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
	return YES;
}
/*
-(void) updateDots {
    for (int i = 0; i < [self.pageScroll.subviews count]; i++)
    {
        UIImageView* dot = [self.pageScroll.subviews objectAtIndex:i];
        
        if (i == self.pageControl.currentPage) {
            
                dot.image = [UIImage imageNamed:@"btn_slide_circle_selected.png"];
            
                     
        } else {
                dot.image = [UIImage imageNamed:@"btn_slide_circle.png"];  
        }
    }
}
 */
- (void) initPopUpView {
}

- (void) animatePopUpShow {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationWillStartSelector:@selector(initPopUpView)];

    popup.alpha = 1;
    //float popUpY = popup.frame.size.height;
    //float popUpX = popup.frame.size.width;
    //float viewY = self.view.frame.size.height;
    //float viewX = self.view.frame.size.width;
    popup.frame = CGRectMake (55, 35, 913, 685);
    
    [UIView commitAnimations];
}
- (void) animatePopUpHide {
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3];
    [UIView setAnimationDelegate:self];
    
    popup.alpha = 0;
   
    popup.frame = CGRectMake (55, 35, 0, 0);
    
    [UIView commitAnimations];
}

-(IBAction)showPopUp:(id)sender{
    [self animatePopUpShow];
    
}
-(IBAction)hidePopUp:(id)sender{
    [self animatePopUpHide];
    
}
-(IBAction)BackHome:(id)sender{
    ViewController *view = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    view.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:view animated:YES completion:nil];
   // [view release];
    
}
-(IBAction)T360:(id)sender{
    T360Controller *view = [[T360Controller alloc] initWithNibName:@"T360Controller" bundle:nil];
    view.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    [self presentViewController:view animated:YES completion:nil];
    //[view release];
    
}


@end

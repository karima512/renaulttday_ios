//
//  MenuLeftViewController.m
//  RenaultTday
//
//  Created by MacBook Pro on 05/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import "MenuRightViewControllerDG3.h"
#import "TavantController.h"
#import "T360Controller.h"
#import "TarriereController.h"
#import "TbordController.h"
#import "TcockController.h"
#import "TpassagerController.h"
#import "TconducteurController.h"
#import "CKavantController.h"
#import "CK360Controller.h"
#import "CKarriereController.h"
#import "CKbordController.h"
#import "CKcockController.h"
#import "CKpassagerController.h"
#import "CKconducteurController.h"
#import "DavantController.h"
#import "DarriereController.h"
#import "DCockBordController.h"
#import "DpassagerController.h"
#import "DconducteurController.h"

@interface MenuRightViewControllerDG3 ()
@end
@implementation MenuRightViewControllerDG3
@synthesize labeltext1, labeltext4, labeltext2, labeltext7, labeltext6, labeltext3,labeltext5, imageHover1, imageHover4,labeldestext1,labeldestext4,labeldestext2,labeldestext7,labeldestext6,labeldestext5,labeldestext3, imageHover2, imageHover7, imageHover6, imageHover3,imageHover5;
@synthesize image1,image4,image2,image7,image6,image5,image3;
@synthesize button1, button2, button3,button4,button5,button6,button7;

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Release any cached data, images, etc that aren't in use.
    NSLog(@"full memory");
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    NSUserDefaults *__autoreleasing defaults = [NSUserDefaults standardUserDefaults];
    NSString  * __autoreleasing current_menu = [defaults objectForKey:@"menu"];
    if ([current_menu isEqual: @"360"]) {
        [labeltext1 setTextColor:[UIColor whiteColor]];
        [imageHover1 setHidden:NO];
        [labeltext2 setTextColor:[UIColor blackColor]];
        [imageHover2 setHidden:YES];
        [labeltext4 setTextColor:[UIColor blackColor]];
        [imageHover4 setHidden:YES];
        [labeltext7 setTextColor:[UIColor blackColor]];
        [imageHover7 setHidden:YES];
        [labeltext6 setTextColor:[UIColor blackColor]];
        [imageHover6 setHidden:YES];
        
        [labeltext3 setTextColor:[UIColor blackColor]];
        [imageHover3 setHidden:YES];
        [labeltext5 setTextColor:[UIColor blackColor]];
        [labeldestext2 setTextColor:[UIColor blackColor]];
        [labeldestext4 setTextColor:[UIColor blackColor]];
        [labeldestext7 setTextColor:[UIColor blackColor]];
        [labeldestext6 setTextColor:[UIColor blackColor]];
        [labeldestext5 setTextColor:[UIColor blackColor]];
        [labeldestext3 setTextColor:[UIColor blackColor]];
        
    }
    if ([current_menu isEqual: @"avant"]) {
        [labeltext1 setTextColor:[UIColor blackColor]];
        [imageHover1 setHidden:YES];
        [labeltext2 setTextColor:[UIColor blackColor]];
        [imageHover2 setHidden:YES];
        [labeltext6 setTextColor:[UIColor whiteColor]];
        [imageHover6 setHidden:NO];
        [labeltext7 setTextColor:[UIColor blackColor]];
        [imageHover7 setHidden:YES];
        [labeltext4 setTextColor:[UIColor blackColor]];
        [imageHover4 setHidden:YES];
        [labeltext3 setTextColor:[UIColor blackColor]];
        [imageHover3 setHidden:YES];
        [labeltext5 setTextColor:[UIColor blackColor]];
        [imageHover5 setHidden:YES];
        
        [labeldestext2 setTextColor:[UIColor blackColor]];
        [labeldestext6 setTextColor:[UIColor whiteColor]];
        [labeldestext7 setTextColor:[UIColor blackColor]];
        [labeldestext4 setTextColor:[UIColor blackColor]];
        [labeldestext5 setTextColor:[UIColor blackColor]];
        [labeldestext3 setTextColor:[UIColor blackColor]];
        
        
    }
    if ([current_menu isEqual: @"arriere"]) {
        [labeltext1 setTextColor:[UIColor blackColor]];
        [imageHover1 setHidden:YES];
        [labeltext6 setTextColor:[UIColor blackColor]];
        [imageHover6 setHidden:YES];
        [labeltext4 setTextColor:[UIColor blackColor]];
        [imageHover4 setHidden:YES];
        [labeltext7 setTextColor:[UIColor blackColor]];
        [imageHover7 setHidden:YES];
        [labeltext2 setTextColor:[UIColor whiteColor]];
        [imageHover2 setHidden:NO];
        [labeltext3 setTextColor:[UIColor blackColor]];
        [imageHover3 setHidden:YES];
        [labeltext5 setTextColor:[UIColor blackColor]];
        [imageHover5 setHidden:YES];
        
        [labeldestext6 setTextColor:[UIColor blackColor]];
        [labeldestext4 setTextColor:[UIColor blackColor]];
        [labeldestext7 setTextColor:[UIColor blackColor]];
        [labeldestext2 setTextColor:[UIColor whiteColor]];
        [labeldestext5 setTextColor:[UIColor blackColor]];
        [labeldestext3 setTextColor:[UIColor blackColor]];
        
        
        
    }
    if ([current_menu isEqual: @"bord"]) {
        [labeltext1 setTextColor:[UIColor blackColor]];
        [imageHover1 setHidden:YES];
        [labeltext2 setTextColor:[UIColor blackColor]];
        [imageHover2 setHidden:YES];
        [labeltext4 setTextColor:[UIColor blackColor]];
        [imageHover4 setHidden:YES];
        [labeltext7 setTextColor:[UIColor blackColor]];
        [imageHover7 setHidden:YES];
        [labeltext6 setTextColor:[UIColor blackColor]];
        [imageHover6 setHidden:YES];
        [labeltext5 setTextColor:[UIColor whiteColor]];
        [imageHover5 setHidden:NO];
        [labeltext3 setTextColor:[UIColor blackColor]];
        [imageHover3 setHidden:YES];
        
        [labeldestext2 setTextColor:[UIColor blackColor]];
        [labeldestext4 setTextColor:[UIColor blackColor]];
        [labeldestext7 setTextColor:[UIColor blackColor]];
        [labeldestext6 setTextColor:[UIColor blackColor]];
        [labeldestext3 setTextColor:[UIColor blackColor]];
        [labeldestext5 setTextColor:[UIColor whiteColor]];
        
    }
    if ([current_menu isEqual: @"cock"]) {
        [labeltext1 setTextColor:[UIColor blackColor]];
        [imageHover1 setHidden:YES];
        [labeltext4 setTextColor:[UIColor whiteColor]];
        [imageHover4 setHidden:NO];
        [labeltext2 setTextColor:[UIColor blackColor]];
        [imageHover2 setHidden:YES];
        [labeltext7 setTextColor:[UIColor blackColor]];
        [imageHover7 setHidden:YES];
        [labeltext6 setTextColor:[UIColor blackColor]];
        [imageHover6 setHidden:YES];
        [labeltext3 setTextColor:[UIColor blackColor]];
        [imageHover3 setHidden:YES];
        [labeltext5 setTextColor:[UIColor blackColor]];
        [imageHover5 setHidden:YES];
        
        [labeldestext4 setTextColor:[UIColor whiteColor]];
        [labeldestext2 setTextColor:[UIColor blackColor]];
        [labeldestext7 setTextColor:[UIColor blackColor]];
        [labeldestext6 setTextColor:[UIColor blackColor]];
        [labeldestext5 setTextColor:[UIColor blackColor]];
        [labeldestext3 setTextColor:[UIColor blackColor]];
        
        
        
    }
    if ([current_menu isEqual: @"passager"]) {
        [labeltext1 setTextColor:[UIColor blackColor]];
        [imageHover1 setHidden:YES];
        [labeltext2 setTextColor:[UIColor blackColor]];
        [imageHover2 setHidden:YES];
        [labeltext4 setTextColor:[UIColor blackColor]];
        [imageHover4 setHidden:YES];
        [labeltext5 setTextColor:[UIColor blackColor]];
        [imageHover5 setHidden:YES];
        [labeltext6 setTextColor:[UIColor blackColor]];
        [imageHover6 setHidden:YES];
        [labeltext7 setTextColor:[UIColor whiteColor]];
        [imageHover7 setHidden:NO];
        [labeltext3 setTextColor:[UIColor blackColor]];
        [imageHover3 setHidden:YES];
        
        [labeldestext2 setTextColor:[UIColor blackColor]];
        [labeldestext4 setTextColor:[UIColor blackColor]];
        [labeldestext5 setTextColor:[UIColor blackColor]];
        [labeldestext6 setTextColor:[UIColor blackColor]];
        [labeldestext3 setTextColor:[UIColor blackColor]];
        [labeldestext7 setTextColor:[UIColor whiteColor]];
        
        
    }
    if ([current_menu isEqual: @"conducteur"]) {
        [labeltext1 setTextColor:[UIColor blackColor]];
        [imageHover1 setHidden:YES];
        [labeltext2 setTextColor:[UIColor blackColor]];
        [imageHover2 setHidden:YES];
        [labeltext4 setTextColor:[UIColor blackColor]];
        [imageHover4 setHidden:YES];
        [labeltext5 setTextColor:[UIColor blackColor]];
        [imageHover5 setHidden:YES];
        [labeltext6 setTextColor:[UIColor blackColor]];
        [imageHover6 setHidden:YES];
        [labeltext7 setTextColor:[UIColor blackColor]];
        [imageHover7 setHidden:YES];
        [labeltext3 setTextColor:[UIColor whiteColor]];
        [imageHover3 setHidden:NO];
        
        [labeldestext2 setTextColor:[UIColor blackColor]];
        [labeldestext4 setTextColor:[UIColor blackColor]];
        [labeldestext5 setTextColor:[UIColor blackColor]];
        [labeldestext6 setTextColor:[UIColor blackColor]];
        [labeldestext3 setTextColor:[UIColor whiteColor]];
        [labeldestext7 setTextColor:[UIColor blackColor]];
        
        
    }
    NSString * __autoreleasing current_cat = [defaults objectForKey:@"category"];
    if ([current_cat isEqual: @"D"]) {
        [image2 setImage:[UIImage imageNamed:@"menuDface-arriere.png"]];
        [image6 setImage:[UIImage imageNamed:@"menuDface-avant.png"]];
        [image5 setImage:[UIImage imageNamed:@"menuDface-vie.png"]];
        [image3 setImage:[UIImage imageNamed:@"menuDface-conducteur.png"]];
        [image7 setImage:[UIImage imageNamed:@"menuDface-passager.png"]];
        [image4 setImage:[UIImage imageNamed:@"menuDface-cockpit.png"]];
        
        /*[button4 addTarget:self action:@selector(goToCock) forControlEvents:UIControlEventTouchUpInside];
         [button5 addTarget:self action:@selector(goToAvant) forControlEvents:UIControlEventTouchUpInside];
         [button6 addTarget:self action:@selector(goToPassager) forControlEvents:UIControlEventTouchUpInside];
         [button2 addTarget:self action:@selector(goToArriere) forControlEvents:UIControlEventTouchUpInside];
         [button3 addTarget:self action:@selector(goToConducteur) forControlEvents:UIControlEventTouchUpInside];*/
        
        [labeltext1 setHidden:YES];
        [imageHover1 setHidden:YES];
        [button1 setHidden:YES];
        [image1 setHidden:YES];
        
        /*[labeltext7 setHidden:YES];
         [labeldestext7 setHidden:YES];
         [image7 setHidden:YES];
         [button7 setHidden:YES];*/
        
    }
    
    if ([current_cat isEqual: @"CK"]) {
        [image2 setImage:[UIImage imageNamed:@"menu-CKface-arriere.png"]];
        [image6 setImage:[UIImage imageNamed:@"menu-CKface-avant.png"]];
        [image3 setImage:[UIImage imageNamed:@"menu-CKface-conducteur.png"]];
        [image7 setImage:[UIImage imageNamed:@"menu-CKface-passager.png"]];
        [image1 setImage:[UIImage imageNamed:@"menu-CKface360.png"]];
        [image5 setImage:[UIImage imageNamed:@"menuDface-vie.png"]];
        [image4 setImage:[UIImage imageNamed:@"menuDface-cockpit.png"]];
        
        
        
    }
    
    
    
    
}
- (UIViewController *)viewController {
    UIResponder *responder = self;
    while (![responder isKindOfClass:[UIViewController class]]) {
        responder = [responder nextResponder];
        if (nil == responder) {
            break;
        }
    }
    return (UIViewController *)responder;
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
}

- (void)viewDidDisappear:(BOOL)animated
{
	[super viewDidDisappear:animated];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

-(IBAction)goToAvant:(id)sender{
    NSUserDefaults *__autoreleasing defaults = [NSUserDefaults standardUserDefaults];
    NSString * __autoreleasing menu = @"avant";
    [defaults setObject:menu forKey:@"menu"];
    [defaults synchronize];
    NSLog(@"click avant");
    NSString * __autoreleasing current_cat = [defaults objectForKey:@"category"];
    if ([current_cat isEqual: @"T"]) {
        TavantController *__autoreleasing Tavview = [[TavantController alloc] initWithNibName:@"TavantController" bundle:nil];
        Tavview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tavview;
        //[self.parentViewController presentViewController:Tavview animated:YES completion:nil];
        Tavview = nil;
    }
    if ([current_cat isEqual: @"CK"]) {
        CKavantController *__autoreleasing Tavview = [[CKavantController alloc] initWithNibName:@"CKavantController" bundle:nil];
        Tavview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tavview;
        //[self.parentViewController presentViewController:Tavview animated:YES completion:nil];
        Tavview = nil;
        
    }
    if ([current_cat isEqual: @"D"]) {
        DavantController *__autoreleasing Tavview = [[DavantController alloc] initWithNibName:@"DavantController" bundle:nil];
        Tavview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tavview;
        //[self.parentViewController presentViewController:Tavview animated:YES completion:nil];
        Tavview = nil;
        
    }
    
    
}
-(IBAction)goToArriere:(id)sender{
    NSUserDefaults *__autoreleasing defaults = [NSUserDefaults standardUserDefaults];
    NSString *__autoreleasing menu = @"arriere";
    [defaults setObject:menu forKey:@"menu"];
    [defaults synchronize];
    NSString * __autoreleasing current_cat = [defaults objectForKey:@"category"];
    if ([current_cat isEqual: @"T"]) {
        TarriereController *__autoreleasing Tarrview = [[TarriereController alloc] initWithNibName:@"TarriereController" bundle:nil];
        Tarrview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tarrview;
        //[self.parentViewController presentViewController:Tarrview animated:YES completion:nil];
        
        Tarrview = nil;
        
    }
    if ([current_cat isEqual: @"CK"]) {
        CKarriereController *__autoreleasing Tarrview = [[CKarriereController alloc] initWithNibName:@"CKarriereController" bundle:nil];
        Tarrview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tarrview;
        //[self.parentViewController presentViewController:Tarrview animated:YES completion:nil];
        Tarrview = nil;
        
    }
    if ([current_cat isEqual: @"D"]) {
        DarriereController *__autoreleasing Tarrview = [[DarriereController alloc] initWithNibName:@"DarriereController" bundle:nil];
        Tarrview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tarrview;
        //[self.parentViewController presentViewController:Tarrview animated:YES completion:nil];
        Tarrview = nil;
        
        
    }
    
    
}
-(IBAction)goTo360:(id)sender{
    NSUserDefaults *__autoreleasing defaults = [NSUserDefaults standardUserDefaults];
    NSString *menu = @"360";
    [defaults setObject:menu forKey:@"menu"];
    [defaults synchronize];
    NSLog(@"click 360");
    
    NSString *current_cat = [defaults objectForKey:@"category"];
    if ([current_cat isEqual: @"T"]) {
        T360Controller *__autoreleasing T360view = [[T360Controller alloc] initWithNibName:@"T360Controller" bundle:nil];
        T360view.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = T360view;
        //[self.parentViewController presentViewController:T360view animated:YES completion:nil];
        T360view = nil;
        
        
        
    }
    if ([current_cat isEqual: @"CK"]) {
        CK360Controller * __autoreleasing T360view = [[CK360Controller alloc] initWithNibName:@"CK360Controller" bundle:nil];
        T360view.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = T360view;
        
        //[self.parentViewController presentViewController:T360view animated:YES completion:nil];
        T360view = nil;
        
    }
    if ([current_cat isEqual: @"D"]) {
        
    }
    
}

-(IBAction)goToBord:(id)sender{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *menu = @"bord";
    [defaults setObject:menu forKey:@"menu"];
    [defaults synchronize];
    
    NSString *current_cat = [defaults objectForKey:@"category"];
    if ([current_cat isEqual: @"T"]) {
        TbordController *__autoreleasing Tbordview = [[TbordController alloc] initWithNibName:@"TbordController" bundle:nil];
        Tbordview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tbordview;
        
        //[self.parentViewController presentViewController:Tbordview animated:YES completion:nil];
        Tbordview = nil;
        
        
    }
    if ([current_cat isEqual: @"CK"]) {
        CKbordController *__autoreleasing Tbordview = [[CKbordController alloc] initWithNibName:@"CKbordController" bundle:nil];
        Tbordview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tbordview;
        
        //[self.parentViewController presentViewController:Tbordview animated:YES completion:nil];
        Tbordview = nil;
        
    }
    if ([current_cat isEqual: @"D"]) {
        DcockBordController *__autoreleasing Tbordview = [[DcockBordController alloc] initWithNibName:@"DcockBordController" bundle:nil];
        Tbordview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tbordview;
        
        //[self.parentViewController presentViewController:Tbordview animated:YES completion:nil];
        Tbordview = nil;
        
    }
    
}
-(IBAction)goToCock:(id)sender{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *menu = @"cock";
    [defaults setObject:menu forKey:@"menu"];
    [defaults synchronize];
    
    NSString *current_cat = [defaults objectForKey:@"category"];
    if ([current_cat isEqual: @"T"]) {
        TcockController *__autoreleasing Tcocview = [[TcockController alloc] initWithNibName:@"TcockController" bundle:nil];
        Tcocview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tcocview;
        
        //[self.parentViewController presentViewController:Tcocview animated:YES completion:nil];
        Tcocview = nil;
        
        
    }
    if ([current_cat isEqual: @"CK"]) {
        CKcockController *__autoreleasing Tcocview = [[CKcockController alloc] initWithNibName:@"CKcockController" bundle:nil];
        Tcocview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tcocview;
        
        //[self.parentViewController presentViewController:Tcocview animated:YES completion:nil];
        Tcocview = nil;
        
    }
    if ([current_cat isEqual: @"D"]) {
        DcockBordController *__autoreleasing Tbordview = [[DcockBordController alloc] initWithNibName:@"DcockBordController" bundle:nil];
        Tbordview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tbordview;
        
        //[self.parentViewController presentViewController:Tbordview animated:YES completion:nil];
        Tbordview = nil;
        
        
    }
    
}
-(IBAction)goToPassager:(id)sender{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *menu = @"passager";
    [defaults setObject:menu forKey:@"menu"];
    [defaults synchronize];
    
    NSString *current_cat = [defaults objectForKey:@"category"];
    if ([current_cat isEqual: @"T"]) {
        TpassagerController *__autoreleasing Tpassview = [[TpassagerController alloc] initWithNibName:@"TpassagerController" bundle:nil];
        Tpassview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tpassview;
        
        //[self.parentViewController presentViewController:Tpassview animated:YES completion:nil];
        Tpassview = nil;
        
        
    }
    if ([current_cat isEqual: @"CK"]) {
        CKpassagerController *__autoreleasing Tpassview = [[CKpassagerController alloc] initWithNibName:@"CKpassagerController" bundle:nil];
        Tpassview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tpassview;
        
        //[self.parentViewController presentViewController:Tpassview animated:YES completion:nil];
        Tpassview = nil;
        
    }
    if ([current_cat isEqual: @"D"]) {
        DpassagerController *__autoreleasing Tpassview = [[DpassagerController alloc] initWithNibName:@"DpassagerController" bundle:nil];
        Tpassview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tpassview;
        
        //[self.parentViewController presentViewController:Tpassview animated:YES completion:nil];
        Tpassview = nil;
        
        
    }
    
}
-(IBAction)goToConducteur:(id)sender{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *menu = @"conducteur";
    [defaults setObject:menu forKey:@"menu"];
    [defaults synchronize];
    
    NSString *current_cat = [defaults objectForKey:@"category"];
    if ([current_cat isEqual: @"T"]) {
        TconducteurController *__autoreleasing Tconview = [[TconducteurController alloc] initWithNibName:@"TconducteurController" bundle:nil];
        Tconview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tconview;
        
        //[self.parentViewController presentViewController:Tconview animated:YES completion:nil];
        Tconview = nil;
        
        
    }
    if ([current_cat isEqual: @"CK"]) {
        CKconducteurController *__autoreleasing Tconview = [[CKconducteurController alloc] initWithNibName:@"CKconducteurController" bundle:nil];
        Tconview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [UIApplication sharedApplication].delegate.window.rootViewController = Tconview;
        
        //[self.parentViewController presentViewController:Tconview animated:YES completion:nil];
        Tconview = nil;
        
    }
    if ([current_cat isEqual: @"D"]) {
        DconducteurController *__autoreleasing Tconview = [[DconducteurController alloc] initWithNibName:@"DconducteurController" bundle:nil];
        Tconview.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        //[self.parentViewController presentViewController:Tconview animated:YES completion:nil];
        [UIApplication sharedApplication].delegate.window.rootViewController = Tconview;
        
        Tconview = nil;
        
        
    }
    
}



@end

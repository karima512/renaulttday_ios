//
//  GroupController.h
//  RenaultTday
//
//  Created by Karim Jebali on 02/12/13.
//
//

#import <UIKit/UIKit.h>

@interface GroupController : UIViewController

-(IBAction)group1:(id)sender;
-(IBAction)group2:(id)sender;
-(IBAction)group3:(id)sender;

@end

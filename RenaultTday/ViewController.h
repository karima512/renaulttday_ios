//
//  ViewController.h
//  RenaultTday
//
//  Created by MacBook Pro on 01/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class ViewController;
@interface ViewController : UIViewController<NSXMLParserDelegate>{
}
@property (nonatomic, strong) NSMutableString *currentElement;

@property (strong, nonatomic) IBOutlet UIButton *logout;
- (IBAction) frenshClick: (id)sender;
- (IBAction) englishClick: (id)sender;
- (IBAction) deconnect:(id)sender;
@end

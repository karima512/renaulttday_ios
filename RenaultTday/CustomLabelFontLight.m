//
//  CustomLabelFontLight.m
//  RenaultTday
//
//  Created by Karim Jebali on 02/11/13.
//
//

#import "CustomLabelFontLight.h"

@implementation CustomLabelFontLight

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    self.font = [UIFont fontWithName:@"DIN-Light"
                                size:self.font.pointSize];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end

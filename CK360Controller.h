//
//  CK360Controller.h
//  RenaultTday
//
//  Created by MacBook Pro on 06/10/13.
//  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
//
//

#import <UIKit/UIKit.h>
#import "FVImageSequence.h"

@interface CK360Controller : UIViewController{
	IBOutlet FVImageSequence *imageSquence1;
	IBOutlet FVImageSequence *imageSquence2;
    IBOutlet UIButton *LeftButton;
    IBOutlet UIButton *RightButton;
    int current;
	int previous;
    BOOL GammeC;
    BOOL leftShown;
    BOOL rightShown;
}
@property (strong, nonatomic) IBOutlet UILabel *textAteliers;
@property (strong, nonatomic) IBOutlet UILabel *textMenus;
@property (strong, nonatomic) IBOutlet UILabel *textIndex;
@property (strong, nonatomic) IBOutlet UIView *leftMenu;
@property (strong, nonatomic) IBOutlet UIView *rightMenu;
@property (strong, nonatomic) IBOutlet UIImageView *bgOpacity;
@property (strong, nonatomic) IBOutlet UIView *viewC;
@property (strong, nonatomic) IBOutlet UIView *viewK;
@property (strong, nonatomic) IBOutlet UIButton *change;



- (IBAction) BackHome: (id)sender;
- (IBAction) MenuRight: (id)sender;
- (IBAction) MenuLeft: (id)sender;
- (IBAction) changeView: (id)sender;

@end

